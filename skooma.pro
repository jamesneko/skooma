# QMake project file for QProcess.

TEMPLATE		=	app
TARGET		=	skooma

CONFIG		+=	qt debug
QT				+= xml

# WINDOS SPECIFIC HACKS
win32 {
	# Removal of debug_and_release_target makes things more sane when compiling on win32.
	CONFIG		-= debug_and_release_target
	# Always run in release mode since we have to actually distribute Qt libs.
	CONFIG		-= debug
	CONFIG		+= release

	# And of course, with windows, suddenly we have no console output AT ALL
	# unless we add the 'console' flag, which has a side effect of opening an
	# ugly cmd.exe window alongside the main program even if you run it from
	# explorer.exe or a launcher. So dumb. We'll want to comment out this next
	# line if we are making a proper release:
	# Note: Strange error messages appear if you enable this without a make clean.
	CONFIG		+= console

	# I have no idea why '.' is not on the include path by default
	INCLUDEPATH	+= .

	# And again, it seems Qt has trouble finding plugin dlls on windows.
	QTPLUGIN		+= qsvg

	message(QMAKE: Configured for MS/Win32 perhaps why not.)
} else {
	message(QMAKE: Configured for any normal sane build environment.)
}


RESOURCES	=	Resources/skooma.qrc
RCC_DIR		=	Resources/
MOC_DIR		=	qmake-moc/
UI_DIR		=	qmake-ui/
OBJECTS_DIR	=	qmake-o/


SOURCES		+=	main.cc

# ================ MODEL ================ #

HEADERS		+= Model/AbstractRecord.h
HEADERS		+= Model/File.h
SOURCES		+= Model/File.cc
HEADERS		+= Model/RecordVisitor.h
HEADERS		+= Model/IdIndex.h
HEADERS		+= Model/RecordCollection.h
SOURCES		+= Model/RecordCollection.cc
HEADERS		+= Model/RecordFormats.h
SOURCES		+= Model/RecordFormats.cc

# ================ TES3 ================ #

HEADERS		+=	TES3/Exceptions.h
HEADERS		+=	TES3/Record.h
HEADERS		+=	TES3/SubRecord.h

HEADERS		+=	TES3/FloatSubRecord.h
HEADERS		+=	TES3/HeaderSubRecord.h
HEADERS		+=	TES3/Int16SubRecord.h
HEADERS		+=	TES3/Int32SubRecord.h
HEADERS		+=	TES3/Int64SubRecord.h
HEADERS		+=	TES3/ItemCountSubRecord.h
HEADERS		+=	TES3/ScriptHeaderSubRecord.h
HEADERS		+=	TES3/StringSubRecord.h
HEADERS		+=	TES3/UninterpretedSubRecord.h

HEADERS		+=	TES3/Reader.h
SOURCES		+=	TES3/Reader.cc
HEADERS		+=	TES3/ReaderState.h
SOURCES		+=	TES3/ReaderState.cc
HEADERS		+= TES3/RecordFormat.h
SOURCES		+= TES3/RecordFormat.cc

# ================ VISITORS ================ #

HEADERS		+= Visitors/ToQVariantVisitor.h
SOURCES		+= Visitors/ToQVariantVisitor.cc
HEADERS		+= Visitors/GetIdVisitor.h
SOURCES		+= Visitors/GetIdVisitor.cc

# ================ UTILS ================ #

HEADERS		+= Utils/AsciiText.h
HEADERS		+= Utils/FileIO.h
SOURCES		+= Utils/FileIO.cc

# ================ GUI ================ #

FORMS			+=	Gui/MainWindow.ui
HEADERS		+=	Gui/MainWindow.h
SOURCES		+=	Gui/MainWindow.cc

HEADERS		+=	Gui/Dialogs.h
SOURCES		+=	Gui/Dialogs.cc

FORMS			+= Gui/FileControls.ui
HEADERS		+= Gui/FileControls.h
SOURCES		+= Gui/FileControls.cc

FORMS			+= Gui/AboutDialog.ui
HEADERS		+= Gui/AboutDialog.h
SOURCES		+= Gui/AboutDialog.cc

FORMS			+= Gui/GrepDialog.ui
HEADERS		+= Gui/GrepDialog.h
SOURCES		+= Gui/GrepDialog.cc

FORMS			+= Gui/InspectorHexWindow.ui
HEADERS		+= Gui/InspectorHexWindow.h
SOURCES		+= Gui/InspectorHexWindow.cc
FORMS			+= Gui/InspectorHexSampleDialog.ui
HEADERS		+= Gui/InspectorHexSampleDialog.h
SOURCES		+= Gui/InspectorHexSampleDialog.cc

FORMS			+=	Gui/RawRecordsWidget.ui
HEADERS		+=	Gui/RawRecordsWidget.h
SOURCES		+=	Gui/RawRecordsWidget.cc

HEADERS		+=	Gui/RawRecordsModel.h
SOURCES		+=	Gui/RawRecordsModel.cc
HEADERS		+=	Gui/RecordsFilterModel.h
SOURCES		+=	Gui/RecordsFilterModel.cc




# MAGIC!

system(svnversion) {
	SVNVERSION = $$system(svnversion)
	message(QMAKE: SVN revision is $$SVNVERSION)
	DEFINES += SVN_HEAD_REVISION="\"\\\"$$SVNVERSION\\\"\""
} else {
	message(QMAKE: SVN revision cannot be determined.)
	DEFINES += SVN_HEAD_REVISION="\"\\\"<svnversion not available>\\\"\""
}


