/**
 * $Id: IdIndex.h 19 2008-08-11 15:16:03Z james_neko $
 * $Revision: 19 $
 * $Date: 2008-08-12 01:16:03 +1000 (Tue, 12 Aug 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MODEL_IDINDEX_H
#define MODEL_IDINDEX_H


#include <map>


namespace Model
{

	/**
	 * Template for creating an index of things, to share the memory used by those things
	 * and replace them with an integer index to this class.
	 *
	 * INDEX_T should be an integer type of the desired size,
	 * IDENTIFIER_T should be your identifier type (e.g. QByteArray).
	 */
	template<class INDEX_T, class IDENTIFIER_T>
	class IdIndex
	{
	public:
		
		typedef map<INDEX_T, IDENTIFIER_T> index_to_identifier_map_type;
		typedef map<IDENTIFIER_T, INDEX_T> identifier_to_index_map_type;
	
		explicit
		IdIndex():
				d_next_index(0);
		{  }
		
		virtual
		~IdIndex()
		{  }
		
		
		/**
		 * Gives you an index for your identifier, possibly adding the
		 * identifier to the internal maps if necessary.
		 *
		 * FIXME: Should throw exception if you exhaust your index space.
		 */
		INDEX_T
		create_index(
				const IDENTIFIER_T &identifier)
		{
			identifier_to_index_map_type::const_iterator found = d_identifier_to_index_map.find(identifier);
			
			if (found != d_identifier_to_index_map.end()) {
				// Found an existing entry, return previously created index.
				return found->second;
			} else {
				// An identifier we haven't seen yet. Add it.
				return add_to_index(identifier);
			}
		}
		
		
		/**
		 * Checks the map to find the original identifier associated
		 * with an index.
		 *
		 * FIXME: Should throw exception if given index does not exist.
		 */
		const IDENTIFIER_T &
		lookup_identifier(
				INDEX_T index)
		{
			index_to_identifier_map_type::const_iterator found = d_index_to_identifier_map.find(index);
			
			if (found != d_index_to_identifier_map.end()) {
				// Found an existing entry, return reference to identifier.
				return found->second;
			} else {
				// How the hell did you get that index?!
				throw "IdIndex::lookup_identifier(): How the hell did you get that index?!";
			}
		}

	protected:
		
		/**
		 * Add the given identifier to the internal maps, assuming
		 * they have already been checked and the identifier does
		 * not already have an index.
		 */
		INDEX_T
		add_to_index(
				const IDENTIFIER_T &identifier)
		{
			d_identifier_to_index_map[identifier] = d_next_index;
			d_index_to_identifier_map[d_next_index] = identifier;
			return d_next_index++;
		}
		
		
		/**
		 * The next index to be assigned.
		 */
		INDEX_T d_next_index;
		
		/**
		 * The map allowing fast lookups of identifier, given an index.
		 */
		index_to_identifier_map_type d_index_to_identifier_map;
		
		/**
		 * The map allowing fast lookups of index, given an identifier.
		 */
		identifier_to_index_map_type d_identifier_to_index_map;
	
	};
}


#endif // MODEL_IDINDEX_H

