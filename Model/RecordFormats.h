/**
 * $Id: RecordFormats.h 42 2011-06-10 10:15:10Z james_neko $
 * $Revision: 42 $
 * $Date: 2011-06-10 20:15:10 +1000 (Fri, 10 Jun 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MODEL_RECORDFORMATS_H
#define MODEL_RECORDFORMATS_H

#include <QScopedPointer>
#include <QObject>	// for Q_DISABLE_COPY

// Pre-declare to avoid include overload.
namespace TES3
{
	class RecordFormat;
}

namespace Model
{
	/**
	 * Stores all the various formats' RecordFormat objects and instantiates them with
	 * XML from our resources.
	 */
	class RecordFormats
	{
	protected:
		explicit
		RecordFormats();
		
	public:
		
		static
		RecordFormats &
		instance()
		{
			if ( ! s_instance) {
				s_instance.reset(new RecordFormats());
			}
			return *s_instance;
		}
		
		virtual
		~RecordFormats()
		{  }
		
		
		TES3::RecordFormat &
		get_tes3_record_format();
		
	private:
		Q_DISABLE_COPY(RecordFormats)
		
		/**
		 * This class is a Singleton.
		 */
		static QScopedPointer<RecordFormats> s_instance;
		
		/*
		 * Here are all the RecordFormat derivations we hold:
		 */
		QScopedPointer<TES3::RecordFormat> d_tes3_record_format;
	};
}

#endif // MODEL_RECORDFORMATS_H

