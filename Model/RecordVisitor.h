/**
 * $Id: RecordVisitor.h 35 2011-02-17 02:42:56Z james_neko $
 * $Revision: 35 $
 * $Date: 2011-02-17 13:42:56 +1100 (Thu, 17 Feb 2011) $ 
 * 
 * Copyright (C) 2008, 2009, 2010, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MODEL_RECORDVISITOR_H
#define MODEL_RECORDVISITOR_H

/**
 * Forward declarations of all the record types this visitor can process.
 * We need forward declarations to avoid some #include spaghetti.
 */
namespace TES3
{
	class Record;

	class FloatSubRecord;
	class HeaderSubRecord;
	class Int16SubRecord;
	class Int32SubRecord;
	class Int64SubRecord;
	class ItemCountSubRecord;
	class ScriptHeaderSubRecord;
	class StringSubRecord;
	class UninterpretedSubRecord;
}


namespace Model
{
	/**
	 * Abstract base for all record visitors.
	 * A visit_* function needs to be set up for each kind of Record/SubRecord that exists.
	 */
	class RecordVisitor
	{
		
	public:
		explicit
		RecordVisitor()
		{  }
		
		virtual
		~RecordVisitor()
		{  }
		
		
		virtual
		void
		visit_tes3_record(TES3::Record &)
		{  }


		virtual
		void
		visit_tes3_float_subrecord(TES3::FloatSubRecord &)
		{  }

		virtual
		void
		visit_tes3_header_subrecord(TES3::HeaderSubRecord &)
		{  }

		virtual
		void
		visit_tes3_int16_subrecord(TES3::Int16SubRecord &)
		{  }

		virtual
		void
		visit_tes3_int32_subrecord(TES3::Int32SubRecord &)
		{  }

		virtual
		void
		visit_tes3_int64_subrecord(TES3::Int64SubRecord &)
		{  }

		virtual
		void
		visit_tes3_item_count_subrecord(TES3::ItemCountSubRecord &)
		{  }

		virtual
		void
		visit_tes3_script_header_subrecord(TES3::ScriptHeaderSubRecord &)
		{  }

		virtual
		void
		visit_tes3_string_subrecord(TES3::StringSubRecord &)
		{  }

		virtual
		void
		visit_tes3_uninterpreted_subrecord(TES3::UninterpretedSubRecord &)
		{  }
		
	};
}



#endif // MODEL_RECORDVISITOR_H

