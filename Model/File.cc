/**
 * $Id: File.cc 34 2011-02-13 07:24:22Z james_neko $
 * $Revision: 34 $
 * $Date: 2011-02-13 18:24:22 +1100 (Sun, 13 Feb 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "File.h"


Model::File::File():
	d(new File_Data)
{  }

Model::File::File(
		const QString &pathname_):
	d(new File_Data)
{
	d->d_fileinfo.setFile(pathname_);
}


Model::File::~File()
{  }


QString
Model::File::filename() const
{
	// Using d-> from a const member function, detach() is not called.
	return d->d_fileinfo.fileName();
}

QString
Model::File::suffix() const
{
	// Using d-> from a const member function, detach() is not called.
	return d->d_fileinfo.suffix();	// Won't handle double-period extensions, dotfiles, etc.
}

QString
Model::File::pathname() const
{
	// Using d-> from a const member function, detach() is not called.
	return d->d_fileinfo.filePath();
}

QString
Model::File::path() const
{
	// Using d-> from a const member function, detach() is not called.
	return d->d_fileinfo.path();
}


Model::RecordCollection::shared_ptr
Model::File::records()
{
	// non-const, detach may be called for this File. Records will be shared anyway though.
	return d->d_records;
}



