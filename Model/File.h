/**
 * $Id: File.h 34 2011-02-13 07:24:22Z james_neko $
 * $Revision: 34 $
 * $Date: 2011-02-13 18:24:22 +1100 (Sun, 13 Feb 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MODEL_FILE_H
#define MODEL_FILE_H

#include <QtGlobal>
#include <QDebug>
#include <QSharedData>
#include <QSharedDataPointer>
#include <QFileInfo>

#include "RecordCollection.h"

namespace Model
{
	/**
	 * Internal data structures of File class.
	 */
	class File_Data :
			public QSharedData
	{
	public:
		
		/**
		 * Shared Data Constructor.
		 * Basic initialisation of data members.
		 */
		File_Data():
				d_records(new RecordCollection)
		{
			qDebug() << "File_Data: Constructed!";
		}
		
		/**
		 * Shared Data Copy-Constructor.
		 */
		File_Data(const File_Data &other):
				QSharedData(other)
		{
			d_fileinfo = other.d_fileinfo;
			d_records = other.d_records;
			qDebug() << "File_Data: Copyconstructed!";
		}
		
		/**
		 * Shared Data Destructor.
		 */
		~File_Data()
		{
			qDebug() << "File_Data: Reference count hit zero, deleting";
		}
		
		/**
		 * File name etc.
		 */
		QFileInfo d_fileinfo;
		
		/**
		 * Canonical instance of RecordCollection for this file.
		 */
		RecordCollection::shared_ptr d_records;
	};
	
	
	/**
	 * Reference-counting class to encapsulate everything associated with a file.
	 * As long as we have this, we have a file loaded in memory.
	 */
	class File
	{
	public:
		
		/**
		 * Construct a new File (and the shared data behind it).
		 */
		explicit
		File();

		explicit
		File(
				const QString &pathname_);
		
		virtual
		~File();
		
		
		/**
		 * Report the basic filename without path elements used to reference this file.
		 */
		QString
		filename() const;

		/**
		 * Report the file suffix/extension that may or may not clue us in to the file type.
		 */
		QString
		suffix() const;

		/**
		 * Report the full (possibly relative) pathname to the file including file name.
		 */
		QString
		pathname() const;

		/**
		 * Report the full (possibly relative) path to the directory containing the file.
		 */
		QString
		path() const;
		
		
		/**
		 * Access the (canonical) RecordCollection for this File.
		 */
		RecordCollection::shared_ptr
		records();
	
	private:
		
		/**
		 * The internal reference-counted data.
		 */
		QSharedDataPointer<File_Data> d;
	};
}



#endif // MODEL_FILE_H

