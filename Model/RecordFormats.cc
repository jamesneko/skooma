/**
 * $Id: RecordFormats.cc 42 2011-06-10 10:15:10Z james_neko $
 * $Revision: 42 $
 * $Date: 2011-06-10 20:15:10 +1000 (Fri, 10 Jun 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QDomDocument>
#include <QFile>
#include "RecordFormats.h"

#include "TES3/RecordFormat.h"


/**
 * Static pointer holding this Singleton instance.
 */
QScopedPointer<Model::RecordFormats> Model::RecordFormats::s_instance;

Model::RecordFormats::RecordFormats()
{  }


TES3::RecordFormat &
Model::RecordFormats::get_tes3_record_format()
{
	if ( ! d_tes3_record_format) {
		QDomDocument dom("TES3");
		QFile file(":/Formats/TES3.xml");
		if ( ! file.open(QIODevice::ReadOnly)) {
			throw QString("Oh shit, where's my TES3.xml?");
		}
		if ( ! dom.setContent(&file)) {
			file.close();
			throw QString("Oh shit, TES3.xml malformed?");
		}
		file.close();
		
		d_tes3_record_format.reset(new TES3::RecordFormat(dom));
	}
	return *d_tes3_record_format;
}

