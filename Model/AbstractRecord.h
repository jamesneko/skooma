/**
 * $Id: AbstractRecord.h 34 2011-02-13 07:24:22Z james_neko $
 * $Revision: 34 $
 * $Date: 2011-02-13 18:24:22 +1100 (Sun, 13 Feb 2011) $ 
 * 
 * Copyright (C) 2008, 2009, 2010 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MODEL_ABSTRACTRECORD_H
#define MODEL_ABSTRACTRECORD_H

// We could make do with a forward declaration, but every derivation of
// AbstractRecord will need to #include RecordVisitor anyway.
#include "RecordVisitor.h"


namespace Model
{
	
	/**
	 * Abstract base for all record type data. This lets us set up visitors for records
	 * and subrecords, both raw, uninterpreted ones and any special ones we set up.
	 */
	class AbstractRecord
	{
		
	public:
		explicit
		AbstractRecord(const QString &type_):
			d_parent_record_ptr(NULL),
			d_record_type(type_)
		{  }
		
		virtual
		~AbstractRecord()
		{  }
		
		
		virtual
		const QString &
		type()
		{
			return d_record_type;
		}
		
		
		virtual
		Model::AbstractRecord *
		parent()
		{
			return d_parent_record_ptr;
		}
		
		virtual
		void
		set_parent(Model::AbstractRecord *parent_)
		{
			d_parent_record_ptr = parent_;
		}
		
		/**
		 * Visitor Pattern kicks ass.
		 *
		 * See the Visitor pattern in Gamma'95 p331.
		 */
		virtual
		void
		accept_visitor(
				Model::RecordVisitor &visitor) = 0;

	protected:
		
		/**
		 * Convenience pointer to help navigate tree structure when used in
		 * RawRecordsModel. May be NULL.
		 */
		AbstractRecord *d_parent_record_ptr;
		
		/**
		 * Convenience QString rendition of the record's type.
		 * FIXME: replace.
		 */
		QString d_record_type;
	};
}



#endif // MODEL_ABSTRACTRECORD_H

