/**
 * $Id: RecordCollection.cc 34 2011-02-13 07:24:22Z james_neko $
 * $Revision: 34 $
 * $Date: 2011-02-13 18:24:22 +1100 (Sun, 13 Feb 2011) $ 
 * 
 * Copyright (C) 2009, 2010 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "RecordCollection.h"


Model::RecordCollection::RecordCollection():
	d(new RecordCollection_Data)
{  }


Model::RecordCollection::~RecordCollection()
{  }


void
Model::RecordCollection::add_record(
	Model::AbstractRecord *record_ptr)
{
	// Using d-> from a non-const member function, detach() is called.
	// We operate on our own view of the shared data if necessary.
	d->d_records.push_back(record_ptr);
}


Model::AbstractRecord *
Model::RecordCollection::at(
	int index)
{
	return d->d_records[index];
}


Model::RecordCollection::records_iterator
Model::RecordCollection::begin()
{
	return d->d_records.begin();
}


Model::RecordCollection::records_iterator
Model::RecordCollection::end()
{
	return d->d_records.end();
}


int
Model::RecordCollection::size() const
{
	// Using d-> from a const member function, detach() is not called.
	return d->d_records.size();
}

