/**
 * $Id: RecordCollection.h 34 2011-02-13 07:24:22Z james_neko $
 * $Revision: 34 $
 * $Date: 2011-02-13 18:24:22 +1100 (Sun, 13 Feb 2011) $ 
 * 
 * Copyright (C) 2009, 2010 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef MODEL_RECORDCOLLECTION_H
#define MODEL_RECORDCOLLECTION_H

#include <QtGlobal>
#include <QtAlgorithms>
#include <QDebug>
#include <QSharedPointer>
#include <QWeakPointer>
#include <QSharedData>
#include <QSharedDataPointer>
#include <QList>

#include "AbstractRecord.h"

namespace Model
{
	/**
	 * Internal data structures of RecordCollection class.
	 */
	class RecordCollection_Data :
			public QSharedData
	{
	public:
		
		typedef QList<AbstractRecord *> records_list_type;
		typedef records_list_type::iterator records_list_iterator;
		typedef records_list_type::const_iterator records_list_const_iterator;
		
		/**
		 * Shared Data Constructor.
		 * Basic initialisation of data members.
		 */
		RecordCollection_Data()
		{
			qDebug() << "RecordCollection_Data: Constructed! I now have"<<d_records.size()<<"records.";
		}
		
		/**
		 * Shared Data Copy-Constructor.
		 */
		RecordCollection_Data(const RecordCollection_Data &other):
				QSharedData(other)
		{
			Q_ASSERT(d_records.isEmpty());
			d_records = other.d_records;		// FIXME: ARE YOU SURE?
			qDebug() << "RecordCollection_Data: Copyconstructed! I now have"<<d_records.size()<<"records.";
		}
		
		/**
		 * Shared Data Destructor.
		 */
		~RecordCollection_Data()
		{
			qDebug() << "RecordCollection_Data: Reference count hit zero, deleting record collection data with"<<d_records.size()<<"records.";
			qDeleteAll(d_records);
			d_records.clear();
		}
		
		/**
		 * The actual list of records in memory.
		 */
		records_list_type d_records;
	};
	
	
	/**
	 * Reference-counting class to encapsulate a bunch of loaded records from a file.
	 *
	 * You can pass this around as you would a QString, and it's safe; the data is
	 * shared. You might want to do that only for temporary ephemeral RecordCollections,
	 * though; the main "store" for a File's *canonical* collection of records should
	 * probably remain a single instance accessed by QSharedPointer (like intrusive_ptr)
	 * or QWeakPointer.
	 */
	class RecordCollection
	{
	public:
		
		typedef QSharedPointer<RecordCollection> shared_ptr;
		typedef QWeakPointer<RecordCollection> weak_ptr;
		typedef RecordCollection_Data::records_list_iterator records_iterator;
		typedef RecordCollection_Data::records_list_const_iterator records_const_iterator;
	
		/**
		 * Construct a new RecordCollection (and the shared data behind it).
		 */
		explicit
		RecordCollection();
		
		virtual
		~RecordCollection();
		
		
		/**
		 * Adds the given record to the RecordCollection.
		 * The RecordCollection takes ownership of the record's memory.
		 */
		void
		add_record(
			Model::AbstractRecord *record_ptr);
		
		
		/**
		 * Returns pointer to an AbstractRecord.
		 * This can modify the record (and the RecordCollection).
		 */
		Model::AbstractRecord *
		at(
			int index);
		
		/**
		 * For iterating over the collection in a potentially modifying way.
		 */
		records_iterator
		begin();

		/**
		 * For iterating over the collection in a potentially modifying way.
		 */
		records_iterator
		end();
		
		// FIXME: Add const iterators for people who want to iterate over
		// a const RecordCollection.
		
		
		/**
		 * Returns the number of records owned by this RecordCollection.
		 */
		int
		size() const;
	
	private:
		
		/**
		 * The internal reference-counted data.
		 */
		QSharedDataPointer<RecordCollection_Data> d;
	};
}



#endif // MODEL_RECORDCOLLECTION_H

