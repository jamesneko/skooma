/**
 * $Id: ReaderState.cc 34 2011-02-13 07:24:22Z james_neko $
 * $Revision: 34 $
 * $Date: 2011-02-13 18:24:22 +1100 (Sun, 13 Feb 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "ReaderState.h"


TES3::ReaderState::ReaderState():
	d_current_record_start(0)
{  }


void
TES3::ReaderState::record_start(
		QByteArray fourchar,
		qint64 pos)
{
	d_current_record = fourchar;
	d_current_record_start = pos;
	clear_record_context();
}


void
TES3::ReaderState::clear_record_context()
{
	d_next_fltv_process_as.clear();
}


void
TES3::ReaderState::subrecord_start(
		QByteArray fourchar,
		qint64 pos)
{
	d_current_subrecord = fourchar;
	d_current_subrecord_start = pos;
}



