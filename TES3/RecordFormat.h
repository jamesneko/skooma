/**
 * $Id: RecordFormat.h 35 2011-02-17 02:42:56Z james_neko $
 * $Revision: 35 $
 * $Date: 2011-02-17 13:42:56 +1100 (Thu, 17 Feb 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TES3_RECORDFORMAT_H
#define TES3_RECORDFORMAT_H

#include <QtGlobal>
#include <QObject>
#include <QMetaObject>
#include <QMetaMethod>
#include <QDomDocument>
#include <QString>
#include <QDataStream>
#include <QByteArray>
#include <QHash>

#include "Exceptions.h"
#include "Record.h"
#include "SubRecord.h"

#include "FloatSubRecord.h"
#include "HeaderSubRecord.h"
#include "Int16SubRecord.h"
#include "Int32SubRecord.h"
#include "Int64SubRecord.h"
#include "ItemCountSubRecord.h"
#include "ScriptHeaderSubRecord.h"
#include "StringSubRecord.h"
#include "UninterpretedSubRecord.h"

#include "Utils/FileIO.h"

namespace TES3
{
	/**
	 * Stores the TES3 Record Format built from the XML file in our resources.
	 * Also holds various small functions to construct TES3 SubRecords.
	 *
	 * Used by TES3::Reader.
	 */
	class RecordFormat :
			public QObject
	{
		Q_OBJECT

	public:
		explicit
		RecordFormat(
				QDomDocument dom);
		
		virtual
		~RecordFormat()
		{  }
		
		
		/**
		 * Call this function to access the subrecord creation dispatch table.
		 */
		QMetaMethod
		lookup_creation_fn(
				const QByteArray &record_fourchar,
				const QByteArray &subrec_fourchar);


		/**
		 * Call this function to invoke the subrecord creation fn given by
		 * lookup_creation_fn().
		 */
		TES3::SubRecord *
		invoke_creation_fn(
				QMetaMethod creation_fn,
				const QByteArray &subrec_fourchar,
				quint32 payload_size,
				QDataStream &data_stream);


		// SubRecord creation functions:-
		// It is important to declare all of these with the Q_INVOKABLE macro.

		/**
		 * Fallback for anything we don't understand. Out of alphabetical order so that
		 * later fns can call it if preconditions fail.
		 */
		Q_INVOKABLE
		TES3::SubRecord *
		create_uninterpreted_subrecord(
				const QByteArray &fourchar,
				quint32 payload_size,
				QDataStream &data_stream)
		{
			// Read raw subrecord payload.
			QByteArray raw_data = Utils::read_raw_data(data_stream, payload_size);
			if (static_cast<quint32>(raw_data.size()) < payload_size) {
				TES3::PrematureEOFException ex = { fourchar };
				throw ex;
			}
			// We may be at EOB here and that is fine.
			return new TES3::UninterpretedSubRecord(fourchar, raw_data);
		}
	
		// All create_x_subrecord() fns are below, ideally in alphabetical order.
	
		Q_INVOKABLE
		TES3::SubRecord *
		create_float_subrecord(
				const QByteArray &fourchar,
				quint32 payload_size,
				QDataStream &data_stream)
		{
			if (payload_size != 4) {
				return create_uninterpreted_subrecord(fourchar, payload_size, data_stream);
			}
			Q_ASSERT(payload_size == 4);
			float f;
			data_stream >> f;
			return new TES3::FloatSubRecord(fourchar, f);
		}
	
		Q_INVOKABLE
		TES3::SubRecord *
		create_header_subrecord(
				const QByteArray &fourchar,
				quint32 payload_size,
				QDataStream &data_stream)
		{
			if (payload_size != 300) {
				return create_uninterpreted_subrecord(fourchar, payload_size, data_stream);
			}
			Q_ASSERT(payload_size == 300);
			Q_ASSERT(sizeof(float) == 4);
			
			float ver;
			data_stream >> ver;
			qint32 unk;			// == is-master-file maybe? Also different for savegames.
			data_stream >> unk;
			QString author = Utils::read_raw_ascii(data_stream, 32);
			QString description = Utils::read_raw_ascii(data_stream, 256);
			qint32 num_records;
			data_stream >> num_records;
			return new TES3::HeaderSubRecord(fourchar, ver, unk, author, description, num_records);
		}
	
		Q_INVOKABLE
		TES3::SubRecord *
		create_int16_subrecord(
				const QByteArray &fourchar,
				quint32 payload_size,
				QDataStream &data_stream)
		{
			// As a very special case, we permit a 4-byte 'short' to be read, to work around the GLOB/FLTV problem. Otherwise, expect 2 bytes only.
			if (payload_size != 2 && payload_size != 4) {
				return create_uninterpreted_subrecord(fourchar, payload_size, data_stream);
			}
			qint16 i;
			data_stream >> i;
			if (payload_size == 4) {	// Hack.
				qint16 hack;
				data_stream >> hack;//not working.... I think??
			}
			return new TES3::Int16SubRecord(fourchar, i);
		}
	
		Q_INVOKABLE
		TES3::SubRecord *
		create_int32_subrecord(
				const QByteArray &fourchar,
				quint32 payload_size,
				QDataStream &data_stream)
		{
			// #### FIXME: die unless payload_size == 4 ... note this FAILS on Tribunal.esm, SakakiManor.esp, and more!
			if (payload_size != 4) {
				return create_uninterpreted_subrecord(fourchar, payload_size, data_stream);
			}
			Q_ASSERT(payload_size == 4);
			// FIXME: I'm assuming it's not an -unsigned- int...
			qint32 i;
			data_stream >> i;
			return new TES3::Int32SubRecord(fourchar, i);
		}
	
		Q_INVOKABLE
		TES3::SubRecord *
		create_int64_subrecord(
				const QByteArray &fourchar,
				quint32 payload_size,
				QDataStream &data_stream)
		{
			if (payload_size != 8) {
				return create_uninterpreted_subrecord(fourchar, payload_size, data_stream);
			}
			Q_ASSERT(payload_size == 8);
			// FIXME: I'm assuming it's not an -unsigned- int64...
			qint64 i;
			data_stream >> i;
			return new TES3::Int64SubRecord(fourchar, i);
		}
	
		Q_INVOKABLE
		TES3::SubRecord *
		create_int_of_some_kind_subrecord(
				const QByteArray &fourchar,
				quint32 payload_size,
				QDataStream &data_stream)
		{
			// Guess what kind of int they want based on payload size. Won't always work...
			switch (payload_size) {
			case 2:	return create_int16_subrecord(fourchar, payload_size, data_stream);
			case 4:	return create_int32_subrecord(fourchar, payload_size, data_stream);
			case 8:	return create_int64_subrecord(fourchar, payload_size, data_stream);
			default:	return create_uninterpreted_subrecord(fourchar, payload_size, data_stream);
			}
		}
	
		Q_INVOKABLE
		TES3::SubRecord *
		create_item_count_subrecord(
				const QByteArray &fourchar,
				quint32 payload_size,
				QDataStream &data_stream)
		{
			// Read raw subrecord payload.
			qint32 c;
			data_stream >> c;
			QByteArray raw_data = Utils::read_raw_data(data_stream, (payload_size - 4));
			if (static_cast<quint32>(raw_data.size()) < (payload_size - 4)) {
				// FIXME: Given read_raw_data's allocation of buffer size, not sure if this will ever trigger. Also, better exception throwing, use Reader state, byte number, what record/subrecord etc.
				TES3::PrematureEOFException ex = { fourchar };
				throw ex;
			}
			// We may be at EOB here and that is fine.
			// FIXME: European versions of Morrowind - do they use ASCII? Latin-1? See also fromLocal8Bit().
			QString string = QString::fromAscii(raw_data.data());
			return new TES3::ItemCountSubRecord(fourchar, c, string);
		}

		Q_INVOKABLE
		TES3::SubRecord *
		create_script_header_subrecord(
				const QByteArray &fourchar,
				quint32 payload_size,
				QDataStream &data_stream)
		{
			if (payload_size != 52) {
				return create_uninterpreted_subrecord(fourchar, payload_size, data_stream);
			}
			Q_ASSERT(payload_size == 52);
			
			QString name = Utils::read_raw_ascii(data_stream, 32);
			qint32 num_shorts, num_longs, num_floats;
			data_stream >> num_shorts >> num_longs >> num_floats;
			qint32 script_data_size, local_var_size;
			data_stream >> script_data_size >> local_var_size;
			return new TES3::ScriptHeaderSubRecord(fourchar, name, num_shorts, num_longs, num_floats, script_data_size, local_var_size);
		}

		Q_INVOKABLE
		TES3::SubRecord *
		create_string_subrecord(
				const QByteArray &fourchar,
				quint32 payload_size,
				QDataStream &data_stream)
		{
			// Read raw subrecord payload.
			QByteArray raw_data = Utils::read_raw_data(data_stream, payload_size);
			if (static_cast<quint32>(raw_data.size()) < payload_size) {
				// FIXME: Given read_raw_data's allocation of buffer size, not sure if this will ever trigger. Also, better exception throwing, use Reader state, byte number, what record/subrecord etc.
				TES3::PrematureEOFException ex = { fourchar };
				throw ex;
			}
			// We may be at EOB here and that is fine.
			// FIXME: European versions of Morrowind - do they use ASCII? Latin-1? See also fromLocal8Bit().
			QString string = QString::fromAscii(raw_data.data());
			return new TES3::StringSubRecord(fourchar, string);
		}
	
		// End of subrecord creation functions.
	
	
		
	private:
		Q_DISABLE_COPY(RecordFormat)
		
		void
		index_creation_fns();

		QString
		correct_creation_fn(
				const QString &fn_name)
		{
			if (d_creation_fn_index.contains(fn_name)) {
				return fn_name;
			} else {
				// FIXME: Warn user that XML is incorrect.
				return "create_uninterpreted_subrecord";
			}
		}

		void
		build_subrecord_dispatch_table();

		QHash<QString, QMetaMethod> d_creation_fn_index;
		QHash<QByteArray, QMetaMethod> d_subrecord_dispatch_table;
		
		QDomDocument d_dom;
	};
}

#endif // TES3_RECORDFORMAT_H

