/**
 * $Id: SubRecord.h 34 2011-02-13 07:24:22Z james_neko $
 * $Revision: 34 $
 * $Date: 2011-02-13 18:24:22 +1100 (Sun, 13 Feb 2011) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TES3_SUBRECORD_H
#define TES3_SUBRECORD_H

#include <QByteArray>
#include <QString>

#include "Model/AbstractRecord.h"


/* Convenience macro for specific SubRecord derivations */
#define BORING_ACCESSORS(typ,nam) \
	typ nam() { return d_##nam ; } \
	void set_##nam(typ new_##nam) { d_##nam = new_##nam ; }
	


namespace TES3
{
	/**
	 * Represents a single mostly abstract subrecord inside an ESP, ESM, or ESS file.
	 */
	class SubRecord :
			public Model::AbstractRecord
	{
		
	public:
		explicit
		SubRecord(
				const QString &type_,
				const QByteArray &fourchar_):
			Model::AbstractRecord(type_),
			d_fourchar(fourchar_)
		{  }
		
		virtual
		~SubRecord()
		{  }
		
		
		virtual
		QByteArray
		fourchar()
		{
			return d_fourchar;
		}
		
	protected:
		
		QByteArray d_fourchar;
		
	};
}



#endif // TES3_SUBRECORD_H
