/**
 * $Id: RecordFormat.cc 34 2011-02-13 07:24:22Z james_neko $
 * $Revision: 34 $
 * $Date: 2011-02-13 18:24:22 +1100 (Sun, 13 Feb 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QDebug>
#include <QDomElement>
#include "RecordFormat.h"



TES3::RecordFormat::RecordFormat(
		QDomDocument dom):
	QObject(NULL),
	d_dom(dom)
{
	qDebug("TES3::RecordFormat instantiated!");
	index_creation_fns();
	build_subrecord_dispatch_table();
}


QMetaMethod
TES3::RecordFormat::lookup_creation_fn(
		const QByteArray &record_fourchar,
		const QByteArray &subrec_fourchar)
{
	// Look for e.g. TES3/DATA first
	QByteArray key(record_fourchar);
	key.append('/').append(subrec_fourchar);
	if (d_subrecord_dispatch_table.contains(key)) {
		return d_subrecord_dispatch_table.value(key);
	} else {
		// Fall back to e.g. */DATA
		key = "*/";
		key.append(subrec_fourchar);
		if (d_subrecord_dispatch_table.contains(key)) {
			return d_subrecord_dispatch_table.value(key);
		} else {
			// Still nothing, just make an uninterpreted record and move on.
			return d_creation_fn_index["create_uninterpreted_subrecord"];
		}
	}
}


TES3::SubRecord *
TES3::RecordFormat::invoke_creation_fn(
		QMetaMethod creation_fn,
		const QByteArray &subrec_fourchar,
		quint32 payload_size,
		QDataStream &data_stream)
{
	TES3::SubRecord *subrec_ptr_rval = NULL;
	bool success = creation_fn.invoke(
			this,
			Qt::DirectConnection,
			Q_RETURN_ARG(TES3::SubRecord *, subrec_ptr_rval),
			Q_ARG(const QByteArray &, subrec_fourchar),
			Q_ARG(quint32, payload_size),
			Q_ARG(QDataStream &, data_stream)
			);
	if ( ! success) {
		qDebug() << "TES3::RecordFormat : Failed to invoke"<<creation_fn.signature();
		return NULL;
	}
	return subrec_ptr_rval;
}




void
TES3::RecordFormat::index_creation_fns()
{
	// Use Qt introspection magic to list all our subrecord creation fns.
	const QMetaObject *meta = metaObject();
	for (int i = meta->methodOffset(); i < meta->methodCount(); ++i) {
		QMetaMethod meth = meta->method(i);
		QString name(meth.signature());
		name = name.left(name.indexOf('('));

		d_creation_fn_index[name] = meth;
	}
}


void
TES3::RecordFormat::build_subrecord_dispatch_table()
{
	QDomElement root = d_dom.documentElement();

	for(QDomNode n = root.firstChild(); !n.isNull(); n = n.nextSibling()) {
		if (n.toElement().tagName() == "RecordType") {
			QDomElement elem_record_type = n.toElement();
			QByteArray curr_record = elem_record_type.attribute("id").toAscii();
			//qDebug() << "TES3::RecordFormat: Processing"<<elem_record_type.attribute("id")<<"("<<elem_record_type.attribute("name")<<")";
			
			for(QDomNode cn = elem_record_type.firstChild(); !cn.isNull(); cn = cn.nextSibling()) {
				if (cn.toElement().tagName() == "SubRecordType") {
					QDomElement elem_subrecord_type = cn.toElement();
					QByteArray curr_subrecord = elem_subrecord_type.attribute("id").toAscii();
					QString subrecord_parser = correct_creation_fn(elem_subrecord_type.attribute("parser"));
					//qDebug() << "                                   "<<elem_subrecord_type.attribute("id")<<" parser="<<subrecord_parser;
					
					QByteArray key(curr_record);
					key.append('/').append(curr_subrecord);
					d_subrecord_dispatch_table[key] = d_creation_fn_index[subrecord_parser];
				}
			}
		}
	}

	// Hackery. You never actually see an INTV or explicit "short" in a GLOB, although you should. This code is used in the pre/post subrecord creation hooks to work around FLTV being used for everything.
	d_subrecord_dispatch_table["GLOB/SHRT"] = d_creation_fn_index["create_int16_subrecord"];
}


