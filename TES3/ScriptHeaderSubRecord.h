/**
 * $Id: ScriptHeaderSubRecord.h 35 2011-02-17 02:42:56Z james_neko $
 * $Revision: 35 $
 * $Date: 2011-02-17 13:42:56 +1100 (Thu, 17 Feb 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TES3_SCRIPTHEADERSUBRECORD_H
#define TES3_SCRIPTHEADERSUBRECORD_H

#include <QByteArray>

#include "TES3/SubRecord.h"


namespace TES3
{
	/**
	 * Represents the HEDR blob subrecord inside an ESP, ESM, or ESS file.
	 */
	class ScriptHeaderSubRecord :
			public TES3::SubRecord
	{
		
	public:
		explicit
		ScriptHeaderSubRecord(
				const QByteArray &fourchar_,
				QString name_,
				qint32 num_shorts_,
				qint32 num_longs_,
				qint32 num_floats_,
				qint32 script_data_size_,
				qint32 local_var_size_):
			TES3::SubRecord("TES3::ScriptHeaderSubRecord", fourchar_),
			d_name(name_),
			d_num_shorts(num_shorts_),
			d_num_longs(num_longs_),
			d_num_floats(num_floats_),
			d_script_data_size(script_data_size_),
			d_local_var_size(local_var_size_)
		{  }

		BORING_ACCESSORS(QString, name)

		BORING_ACCESSORS(qint32, num_shorts)

		BORING_ACCESSORS(qint32, num_longs)

		BORING_ACCESSORS(qint32, num_floats)

		BORING_ACCESSORS(qint32, script_data_size)

		BORING_ACCESSORS(qint32, local_var_size)


		/**
		 * See the Visitor pattern in Gamma'95 p331.
		 */
		virtual
		void
		accept_visitor(
				Model::RecordVisitor &visitor)
		{
			visitor.visit_tes3_script_header_subrecord(*this);
		}
	
	protected:

		QString d_name;
		qint32 d_num_shorts;
		qint32 d_num_longs;
		qint32 d_num_floats;
		qint32 d_script_data_size;
		qint32 d_local_var_size;
		
	};
}



#endif // TES3_HEADERSUBRECORD_H
