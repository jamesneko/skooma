/**
 * $Id: Reader.h 34 2011-02-13 07:24:22Z james_neko $
 * $Revision: 34 $
 * $Date: 2011-02-13 18:24:22 +1100 (Sun, 13 Feb 2011) $ 
 * 
 * Copyright (C) 2008, 2009, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TES3_READER_H
#define TES3_READER_H

#include <QString>
#include <QFile>
#include <QFileInfo>
#include <QList>
#include <QDataStream>
#include <QByteArray>

#include "Model/RecordCollection.h"

#include "ReaderState.h"
#include "Record.h"


namespace TES3
{
	/**
	 * Parser for TES3 data files (ESP, ESM, ESS)
	 */
	class Reader :
			public QObject
	{
		Q_OBJECT
		
	public:

		explicit
		Reader(
				QString filename);
		
		virtual
		~Reader()
		{  }
		
		
		/**
		 * Call to find out the size (in bytes) of the file about to be read.
		 */
		qint64
		size();
		
		/**
		 * Short form of the filename.
		 */
		QString
		filename();
		
		/**
		 * Call to do the actual file reading.
		 */
		Model::RecordCollection
		read();
	
	signals:
	
		void
		read_progress(
				qint64 bytes);
		
	private:
	
		TES3::Record *
		read_header(
				QDataStream &data_stream);

		TES3::Record *
		read_record(
				QDataStream &data_stream);
	
		QList<TES3::SubRecord *>
		read_subrecords(
				const QByteArray &record_fourchar,
				const QByteArray &payload);

		TES3::SubRecord *
		read_subrecord(
				const QByteArray &record_fourchar,
				QDataStream &data_stream);

		
		QFile d_file;
		QFileInfo d_fileinfo;
		QDataStream d_data_stream;
		ReaderState d_state;
		
	};
}



#endif // TES3_READER_H
