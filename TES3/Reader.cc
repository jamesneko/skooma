/**
 * $Id: Reader.cc 34 2011-02-13 07:24:22Z james_neko $
 * $Revision: 34 $
 * $Date: 2011-02-13 18:24:22 +1100 (Sun, 13 Feb 2011) $ 
 * 
 * Copyright (C) 2008, 2009, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


#include <QDebug>
#include <QList>
#include <QHash>
#include <QByteArray>
#include "Reader.h"

#include "Model/RecordFormats.h"

#include "Exceptions.h"
#include "RecordFormat.h"
#include "Record.h"
#include "SubRecord.h"

#include "FloatSubRecord.h"
#include "HeaderSubRecord.h"
#include "Int16SubRecord.h"
#include "Int32SubRecord.h"
#include "Int64SubRecord.h"
#include "StringSubRecord.h"
#include "UninterpretedSubRecord.h"

#include "Utils/FileIO.h"


namespace
{

	/**
	 * Hook called just before subrecord creation, to accomodate hacks that
	 * work around defects in the TES3 format.
	 */
	QMetaMethod
	process_pre_subrecord_hooks(
			QMetaMethod create_subrecord_fn,
			TES3::ReaderState &state_ref)
	{
		// We will lookup creation functions through TES3::RecordFormat.
		static TES3::RecordFormat &record_format = Model::RecordFormats::instance().get_tes3_record_format();

		// Hack 1: Hack around FLTV abuse in GLOBs.
		if (	state_ref.d_current_record == "GLOB" && state_ref.d_current_subrecord == "FLTV"
			&& !state_ref.d_next_fltv_process_as.isEmpty()) {
			// the prior FNAM told us what this FLTV really is.
			create_subrecord_fn = record_format.lookup_creation_fn(
					state_ref.d_current_record, state_ref.d_next_fltv_process_as);
			state_ref.clear_record_context();
		}
		return create_subrecord_fn;
	}
	
	/**
	 * Hook called just after subrecord creation, to accomodate hacks that
	 * work around defects in the TES3 format.
	 *
	 * Bear in mind that if you switch one SubRecord for another here, you
	 * will be leaking memory so clean up the old one. Better yet, just
	 * use the _pre_subrecord_hook() to choose a different creation fn.
	 */
	TES3::SubRecord *
	process_post_subrecord_hooks(
			TES3::SubRecord *subrec,
			TES3::ReaderState &state_ref)
	{
		// Hack 1: Hack around FLTV abuse in GLOBs.
		if (state_ref.d_current_record == "GLOB" && state_ref.d_current_subrecord == "FNAM") {
			// the prior FNAM tells us what the next FLTV really is.
			TES3::StringSubRecord *fnam = dynamic_cast<TES3::StringSubRecord *>(subrec);
			if (fnam) {
				if (fnam->value() == "s") {
					state_ref.d_next_fltv_process_as = "SHRT";	// Made-up.
				} else if (fnam->value() == "l") {
					state_ref.d_next_fltv_process_as = "INTV";
				} else {
					state_ref.d_next_fltv_process_as = "FLTV";
				}
			}
		}
		return subrec;
	}
	
}


TES3::Reader::Reader(
		QString filename):
	d_file(filename),
	d_fileinfo(d_file),
	d_data_stream(&d_file)
{
	//qDebug() << "Initialising TES3::Reader.";
	Utils::make_datastream_sane(d_data_stream);
}


qint64
TES3::Reader::size()
{
	return d_file.size();
}


QString
TES3::Reader::filename()
{
	return d_fileinfo.fileName();
}


Model::RecordCollection
TES3::Reader::read()
{
	Model::RecordCollection records;
	//qDebug() << "Reader: reading" << d_file.fileName();
	emit read_progress(0);

	bool success = d_file.open(QFile::ReadOnly);
	if ( ! success) {
		//qDebug() << "Couldn't open file for reading.";
		TES3::FileErrorException ex = { d_file.error() };
		throw ex;
	}
	
	records.add_record(read_header(d_data_stream));
	while ( ! d_data_stream.atEnd())
	{
		records.add_record(read_record(d_data_stream));
	}
	
	//qDebug() << "Reader: finished reading" << d_file.fileName();
	emit read_progress(d_file.pos());
	return records;
}




TES3::Record *
TES3::Reader::read_header(
		QDataStream &data_stream)
{
	static const QByteArray tes3 = QByteArray("TES3");
	
	// Check TES3.
	QByteArray magic = data_stream.device()->peek(4);
	if (magic != tes3) {
		qDebug() << "This does not look like a TES3 format file - header magic is wrong.";
		TES3::NotTES3FormatException ex = { magic };
		throw ex;
	}
	
	return read_record(data_stream);
}


TES3::Record *
TES3::Reader::read_record(
		QDataStream &data_stream)
{
	// Progress update.
	emit read_progress(d_file.pos());
	qint64 startpos = d_file.pos();
	
	// Read fourchar.
	QByteArray fourchar = Utils::read_fourchar(data_stream);
	d_state.record_start(fourchar, startpos);
	
	// Read size & extended misc data.
	quint32 payload_size, extended1, extended2;
	data_stream >> payload_size >> extended1 >> extended2;
	if (data_stream.atEnd()) {
		TES3::PrematureEOFException ex = { fourchar };
		throw ex;
	}
	//qDebug() << "Read record magic: fourchar =" << fourchar << "payload_size =" << payload_size;
	
	// Read raw record payload.
	QByteArray payload = Utils::read_raw_data(data_stream, payload_size);
	if (static_cast<quint32>(payload.size()) < payload_size) {
		TES3::PrematureEOFException ex = { fourchar };
		throw ex;
	}
	// We may be at EOF here and that is fine.

	// Interpret that into subrecords.
	QList<TES3::SubRecord *> subrecords = read_subrecords(fourchar, payload);
	return new TES3::Record(fourchar, extended1, extended2, subrecords);
}


QList<TES3::SubRecord *>
TES3::Reader::read_subrecords(
		const QByteArray &record_fourchar,
		const QByteArray &payload)
{
	QList<TES3::SubRecord *> subrecords;
	// Create a new QDataStream on the payload of the record - for all the subrecords to use.
	QDataStream subrec_data_stream(payload);	// Convenience constructor that uses QBuffer.
	Utils::make_datastream_sane(subrec_data_stream);
	
	//qDebug() << "Reading subrecords.";
	while ( ! subrec_data_stream.atEnd())
	{
		subrecords << read_subrecord(record_fourchar, subrec_data_stream);
	}
	//qDebug() << "Finished reading subrecords.";
	return subrecords;
}


TES3::SubRecord *
TES3::Reader::read_subrecord(
		const QByteArray &record_fourchar,
		QDataStream &data_stream)
{
	// We will invoke creation functions on TES3::RecordFormat.
	static TES3::RecordFormat &record_format = Model::RecordFormats::instance().get_tes3_record_format();

	// What kind of subrecord is it?
	qint64 startpos = data_stream.device()->pos();
	QByteArray subrec_fourchar = Utils::read_fourchar(data_stream);
	d_state.subrecord_start(subrec_fourchar, startpos);
	if (data_stream.atEnd()) {
		TES3::PrematureEOFException ex = { subrec_fourchar };
		throw ex;
	}

	// Read size.
	quint32 payload_size;
	data_stream >> payload_size;
	if (data_stream.atEnd()) {
		TES3::PrematureEOFException ex = { subrec_fourchar };
		throw ex;
	}
	//qDebug() << "Read subrecord magic: fourchar =" << subrec_fourchar << "payload_size =" << payload_size;
	
	// Remember where the data stream was when we started processing the payload.
	qint64 payload_start_pos = data_stream.device()->pos();
	
	// Use dispatch table to create appropriate subrecord.
	QMetaMethod creation_fn = record_format.lookup_creation_fn(record_fourchar, subrec_fourchar);
	creation_fn = process_pre_subrecord_hooks(creation_fn, d_state);
	TES3::SubRecord *subrec = record_format.invoke_creation_fn(creation_fn, subrec_fourchar, payload_size, data_stream);
	subrec = process_post_subrecord_hooks(subrec, d_state);


	// We might be at the end of the record buffer here, and that is fine; we might not be,
	// meaning more subrecords exist, which will be taken care of in the read_subrecords() loop.

	// However, one thing we should pay attention to is whether the subrecord processing fn
	// actually used up the appropriate amount of buffer.
	qint64 payload_end_pos = data_stream.device()->pos();
	qint64 processed_payload_size = payload_end_pos - payload_start_pos;
	if (processed_payload_size != 0 && processed_payload_size != (qint64)payload_size) {
		if (processed_payload_size > (qint64)payload_size) {
			qDebug() << "TES3::Reader::read_subrecord("<<record_fourchar<<"/"<<subrec_fourchar<<"): Warning, subrecord creation function used up more data than it should ("<<processed_payload_size<<" instead of "<<payload_size<<").";
		} else {
			qDebug() << "TES3::Reader::read_subrecord("<<record_fourchar<<"/"<<subrec_fourchar<<"): Warning, subrecord creation function used up less data than it should ("<<processed_payload_size<<" instead of "<<payload_size<<").";
		}
	}
	
	return subrec;
}


