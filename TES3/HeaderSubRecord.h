/**
 * $Id: HeaderSubRecord.h 34 2011-02-13 07:24:22Z james_neko $
 * $Revision: 34 $
 * $Date: 2011-02-13 18:24:22 +1100 (Sun, 13 Feb 2011) $ 
 * 
 * Copyright (C) 2010 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TES3_HEADERSUBRECORD_H
#define TES3_HEADERSUBRECORD_H

#include <QByteArray>

#include "TES3/SubRecord.h"


namespace TES3
{
	/**
	 * Represents the HEDR blob subrecord inside an ESP, ESM, or ESS file.
	 */
	class HeaderSubRecord :
			public TES3::SubRecord
	{
		
	public:
		explicit
		HeaderSubRecord(
				const QByteArray &fourchar_,
				float version_,
				qint32 unk_,
				QString author_,
				QString description_,
				qint32 num_records_):
			TES3::SubRecord("TES3::HeaderSubRecord", fourchar_),
			d_version(version_),
			d_unk(unk_),
			d_author(author_),
			d_description(description_),
			d_num_records(num_records_)
		{  }

		BORING_ACCESSORS(float, version)

		BORING_ACCESSORS(qint32, unk)

		BORING_ACCESSORS(QString, author)

		BORING_ACCESSORS(QString, description)

		/**
		 * Remember, the num_records here is purely advisory, the header may be wrong.
		 */
		BORING_ACCESSORS(qint32, num_records)

		/**
		 * See the Visitor pattern in Gamma'95 p331.
		 */
		virtual
		void
		accept_visitor(
				Model::RecordVisitor &visitor)
		{
			visitor.visit_tes3_header_subrecord(*this);
		}
	
	protected:

		float d_version;
		qint32 d_unk;
		QString d_author;
		QString d_description;
		qint32 d_num_records;
		
	};
}



#endif // TES3_HEADERSUBRECORD_H
