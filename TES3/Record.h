/**
 * $Id: Record.h 1 2008-06-24 10:52:59Z james_neko $
 * $Revision: 1 $
 * $Date: 2008-06-24 20:52:59 +1000 (Tue, 24 Jun 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TES3_RECORD_H
#define TES3_RECORD_H

#include <QtAlgorithms>
#include <QByteArray>

#include "Model/AbstractRecord.h"
#include "SubRecord.h"


namespace TES3
{
	/**
	 * Represents a single record inside an ESP, ESM, or ESS file.
	 */
	class Record :
			public Model::AbstractRecord
	{
		
	public:
		explicit
		Record(
				const QByteArray &fourchar_,
				const quint32 extended1_,
				const quint32 extended2_,
				const QList<TES3::SubRecord *> &subrecords_):
			Model::AbstractRecord("TES3::Record"),
			d_fourchar(fourchar_),
			d_extended1(extended1_),
			d_extended2(extended2_),
			d_subrecords(subrecords_)
		{
			// Claim ownership of all the subrecords.
			QList<TES3::SubRecord *>::iterator it = d_subrecords.begin();
			QList<TES3::SubRecord *>::iterator end = d_subrecords.end();
			for ( ; it != end; ++it)
			{
				(*it)->set_parent(this);
			}
		}
		
		virtual
		~Record()
		{
			qDeleteAll(d_subrecords);
			d_subrecords.clear();
		}
		
		
		virtual
		QByteArray
		fourchar()
		{
			return d_fourchar;
		}
		
		virtual
		quint32
		extended1()
		{
			return d_extended1;
		}

		virtual
		quint32
		extended2()
		{
			return d_extended2;
		}
		
		virtual
		QList<TES3::SubRecord *> &
		subrecords()
		{
			return d_subrecords;
		}
		
		/**
		 * Visitor Pattern kicks ass.
		 * See the Visitor pattern in Gamma'95 p331.
		 */
		virtual
		void
		accept_visitor(
				Model::RecordVisitor &visitor)
		{
			visitor.visit_tes3_record(*this);
		}

	protected:
		
		QByteArray d_fourchar;
		quint32 d_extended1;
		quint32 d_extended2;
		QList<TES3::SubRecord *> d_subrecords;
		
	};
}



#endif // TES3_RECORD_H
