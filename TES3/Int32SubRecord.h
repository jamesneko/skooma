/**
 * $Id: Int32SubRecord.h 18 2008-07-05 19:50:03Z james_neko $
 * $Revision: 18 $
 * $Date: 2008-07-06 05:50:03 +1000 (Sun, 06 Jul 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TES3_INT32SUBRECORD_H
#define TES3_INT32SUBRECORD_H

#include <QByteArray>

#include "TES3/SubRecord.h"


namespace TES3
{
	/**
	 * Represents a single (signed!) integer-valued subrecord inside an ESP, ESM, or ESS file.
	 */
	class Int32SubRecord :
			public TES3::SubRecord
	{
		
	public:
		explicit
		Int32SubRecord(
				const QByteArray &fourchar_,
				qint32 value_):
			TES3::SubRecord("TES3::Int32SubRecord", fourchar_),
			d_value(value_)
		{  }
		
		qint32
		value()
		{
			return d_value;
		}

		void
		set_value(qint32 new_value)
		{
			d_value = new_value;
		}

		/**
		 * Visitor Pattern kicks ass.
		 * See the Visitor pattern in Gamma'95 p331.
		 */
		virtual
		void
		accept_visitor(
				Model::RecordVisitor &visitor)
		{
			visitor.visit_tes3_int32_subrecord(*this);
		}
	
	protected:
		
		qint32 d_value;
		
	};
}



#endif // TES3_INT32SUBRECORD_H
