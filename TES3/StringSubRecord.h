/**
 * $Id: StringSubRecord.h 1 2008-06-24 10:52:59Z james_neko $
 * $Revision: 1 $
 * $Date: 2008-06-24 20:52:59 +1000 (Tue, 24 Jun 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TES3_STRINGSUBRECORD_H
#define TES3_STRINGSUBRECORD_H

#include <QByteArray>
#include <QString>

#include "TES3/SubRecord.h"


namespace TES3
{
	/**
	 * Represents a single string-valued subrecord inside an ESP, ESM, or ESS file.
	 */
	class StringSubRecord :
			public TES3::SubRecord
	{
		
	public:
		explicit
		StringSubRecord(
				const QByteArray &fourchar_,
				const QString &value_):
			TES3::SubRecord("TES3::StringSubRecord", fourchar_),
			d_value(value_)
		{  }
		
		const QString &
		value()
		{
			return d_value;
		}

		void
		set_value(const QString &new_value)
		{
			// FIXME: You realise, we'll have to do something about CRLFs.
			// And of course, do it in a platform-neutral fashion - Qt might try to
			// second-guess us when we're on MS/Win32.
			d_value = new_value;
		}

		/**
		 * Visitor Pattern kicks ass.
		 * See the Visitor pattern in Gamma'95 p331.
		 */
		virtual
		void
		accept_visitor(
				Model::RecordVisitor &visitor)
		{
			visitor.visit_tes3_string_subrecord(*this);
		}
	
	protected:
		
		QString d_value;
		
	};
}



#endif // TES3_STRINGSUBRECORD_H
