/**
 * $Id: FloatSubRecord.h 34 2011-02-13 07:24:22Z james_neko $
 * $Revision: 34 $
 * $Date: 2011-02-13 18:24:22 +1100 (Sun, 13 Feb 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TES3_FLOATSUBRECORD_H
#define TES3_FLOATSUBRECORD_H

#include "TES3/SubRecord.h"


namespace TES3
{
	/**
	 * Represents a single-precision 4 byte floating point number in an ESS/ESM/ESP file.
	 */
	class FloatSubRecord :
			public TES3::SubRecord
	{
	public:
	
		explicit
		FloatSubRecord(
				const QByteArray &fourchar_,
				float value_):
			TES3::SubRecord("TES3::FloatSubRecord", fourchar_),
			d_value(value_)
		{  }
		
		BORING_ACCESSORS(float, value)

		/**
		 * See the Visitor pattern in Gamma'95 p331.
		 */
		virtual
		void
		accept_visitor(
				Model::RecordVisitor &visitor)
		{
			visitor.visit_tes3_float_subrecord(*this);
		}
	
	protected:
		
		float d_value;		
	};
}

#endif // TES3_FLOATSUBRECORD_H

