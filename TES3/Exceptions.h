/**
 * $Id: Exceptions.h 1 2008-06-24 10:52:59Z james_neko $
 * $Revision: 1 $
 * $Date: 2008-06-24 20:52:59 +1000 (Tue, 24 Jun 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TES3_EXCEPTIONS_H
#define TES3_EXCEPTIONS_H

#include <QByteArray>
#include <QFile>

namespace TES3
{
	/**
	 * "It's okay, it happens to lots of guys."
	 */
	struct PrematureEOFException
	{
		QByteArray record_fourchar;
	};
	
	struct FileErrorException
	{
		QFile::FileError error;
	};
	
	struct NotTES3FormatException
	{
		QByteArray magic;
	};
}



#endif // TES3_EXCEPTIONS_H

