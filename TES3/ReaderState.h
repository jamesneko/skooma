/**
 * $Id: ReaderState.h 34 2011-02-13 07:24:22Z james_neko $
 * $Revision: 34 $
 * $Date: 2011-02-13 18:24:22 +1100 (Sun, 13 Feb 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TES3_READERSTATE_H
#define TES3_READERSTATE_H

#include <QByteArray>



namespace TES3
{
	/**
	 * Small class tracking state during Reader operation.
	 */
	class ReaderState
	{
	public:
		explicit
		ReaderState();

		void
		record_start(
				QByteArray fourchar,
				qint64 pos);
		
		void
		clear_record_context();
		
		void
		subrecord_start(
				QByteArray fourchar,
				qint64 pos);

		// Reader State, for informative purposes:-
		
		QByteArray d_current_record;
		qint64 d_current_record_start;
		
		QByteArray d_current_subrecord;
		qint64 d_current_subrecord_start;	// offset in record payload
		
		// Current Record Context, for hacking around flaws in TES3 format:-
		
		QByteArray d_next_fltv_process_as;
	};
}

#endif // TES3_READERSTATE_H

