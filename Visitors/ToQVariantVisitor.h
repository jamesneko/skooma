/**
 * $Id: ToQVariantVisitor.h 35 2011-02-17 02:42:56Z james_neko $
 * $Revision: 35 $
 * $Date: 2011-02-17 13:42:56 +1100 (Thu, 17 Feb 2011) $ 
 * 
 * Copyright (C) 2008, 2009, 2010, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef VISITORS_TOQVARIANTVISITOR_H
#define VISITORS_TOQVARIANTVISITOR_H

#include <QVariant>
#include "Model/RecordVisitor.h"



namespace Visitors
{
	/**
	 * Visitor to extract printable information from a record/subrecord for
	 * display by the gui model.
	 */
	class ToQVariantVisitor :
			public Model::RecordVisitor
	{
	public:

		/**
		 * What kind of data do you want to pull out?
		 */
		enum Field
		{
			NAME, VALUE
		};


		explicit
		ToQVariantVisitor(
				ToQVariantVisitor::Field field,
				int role /* e.g. Qt::DisplayRole */):
			d_field(field),
			d_role(role)
		{  }
		
		virtual
		~ToQVariantVisitor()
		{  }


		QVariant
		get()
		{
			return d_qv;
		}
		
		
		
		virtual
		void
		visit_tes3_record(
				TES3::Record &tes3_record);


		virtual
		void
		visit_tes3_float_subrecord(
				TES3::FloatSubRecord &tes3_float_subrecord);

		virtual
		void
		visit_tes3_header_subrecord(
				TES3::HeaderSubRecord &tes3_header_subrecord);

		virtual
		void
		visit_tes3_int16_subrecord(
				TES3::Int16SubRecord &tes3_int16_subrecord);

		virtual
		void
		visit_tes3_int32_subrecord(
				TES3::Int32SubRecord &tes3_int32_subrecord);

		virtual
		void
		visit_tes3_int64_subrecord(
				TES3::Int64SubRecord &tes3_int64_subrecord);

		virtual
		void
		visit_tes3_item_count_subrecord(
				TES3::ItemCountSubRecord &tes3_item_count_subrecord);

		virtual
		void
		visit_tes3_script_header_subrecord(
				TES3::ScriptHeaderSubRecord &tes3_script_header_subrecord);

		virtual
		void
		visit_tes3_string_subrecord(
				TES3::StringSubRecord &tes3_string_subrecord);

		virtual
		void
		visit_tes3_uninterpreted_subrecord(
				TES3::UninterpretedSubRecord &tes3_uninterpreted_subrecord);
	
	private:
	
		ToQVariantVisitor::Field d_field;
		int d_role;
		QVariant d_qv;
	};
}



#endif // VISITORS_RECORDVISITOR_H

