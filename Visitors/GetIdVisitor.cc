/**
 * $Id: GetIdVisitor.cc 35 2011-02-17 02:42:56Z james_neko $
 * $Revision: 35 $
 * $Date: 2011-02-17 13:42:56 +1100 (Thu, 17 Feb 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QString>
#include <QList>
#include <QtGlobal>
#include "GetIdVisitor.h"

#include "TES3/Record.h"
#include "TES3/SubRecord.h"

#include "TES3/HeaderSubRecord.h"
#include "TES3/Int32SubRecord.h"
#include "TES3/Int64SubRecord.h"
#include "TES3/ScriptHeaderSubRecord.h"
#include "TES3/StringSubRecord.h"
#include "TES3/UninterpretedSubRecord.h"


void
Visitors::GetIdVisitor::visit_tes3_record(
		TES3::Record &tes3_record)
{
	Q_FOREACH(TES3::SubRecord *subrec, tes3_record.subrecords()) {
		d_current_record_fourchar = tes3_record.fourchar();
		subrec->accept_visitor(*this);
	}
}


void
Visitors::GetIdVisitor::visit_tes3_script_header_subrecord(
		TES3::ScriptHeaderSubRecord &tes3_script_header_subrecord)
{
	d_id = tes3_script_header_subrecord.name();
}


void
Visitors::GetIdVisitor::visit_tes3_string_subrecord(
		TES3::StringSubRecord &tes3_string_subrecord)
{
	if (d_current_record_fourchar == "INFO") {
		if (tes3_string_subrecord.fourchar() == "INAM" && d_id.isNull()) {
			d_id = tes3_string_subrecord.value();
		}
	} else {
		if (tes3_string_subrecord.fourchar() == "NAME" && d_id.isNull()) {
			d_id = tes3_string_subrecord.value();
		}
	}
}



