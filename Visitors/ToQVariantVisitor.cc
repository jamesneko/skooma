/**
 * $Id: ToQVariantVisitor.cc 35 2011-02-17 02:42:56Z james_neko $
 * $Revision: 35 $
 * $Date: 2011-02-17 13:42:56 +1100 (Thu, 17 Feb 2011) $ 
 * 
 * Copyright (C) 2008, 2009, 2010, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QString>
#include <QDebug>
#include "ToQVariantVisitor.h"

#include "GetIdVisitor.h"
#include "Utils/AsciiText.h"

#include "TES3/Record.h"
#include "TES3/SubRecord.h"

#include "TES3/FloatSubRecord.h"
#include "TES3/HeaderSubRecord.h"
#include "TES3/Int16SubRecord.h"
#include "TES3/Int32SubRecord.h"
#include "TES3/Int64SubRecord.h"
#include "TES3/ItemCountSubRecord.h"
#include "TES3/ScriptHeaderSubRecord.h"
#include "TES3/StringSubRecord.h"
#include "TES3/UninterpretedSubRecord.h"


void
Visitors::ToQVariantVisitor::visit_tes3_record(
		TES3::Record &tes3_record)
{
	if (d_field == NAME) {
		d_qv = tes3_record.fourchar();
	} else {
		GetIdVisitor getid;
		tes3_record.accept_visitor(getid);
		if (getid.get().isNull()) {
			d_qv = QString("Record flags: 0x%1, 0x%2").arg(
					QString::number(tes3_record.extended1(), 16),
					QString::number(tes3_record.extended2(), 16)
					);
		} else {
			d_qv = QString("%3 (flags: 0x%1, 0x%2)").arg(
					QString::number(tes3_record.extended1(), 16),
					QString::number(tes3_record.extended2(), 16),
					getid.get()
					);
		}
	}
}


void
Visitors::ToQVariantVisitor::visit_tes3_float_subrecord(
		TES3::FloatSubRecord &tes3_float_subrecord)
{
	if (d_field == NAME) {
		d_qv = tes3_float_subrecord.fourchar();
	} else {
		d_qv = QVariant(tes3_float_subrecord.value());
	}
}

void
Visitors::ToQVariantVisitor::visit_tes3_header_subrecord(
		TES3::HeaderSubRecord &tes3_header_subrecord)
{
	if (d_field == NAME) {
		d_qv = tes3_header_subrecord.fourchar();
	} else {
		d_qv = QString("Ver: %1\nUnk: %2\nAuthor: %3\nDescription: %4\nNumRecords: %5")
				.arg(tes3_header_subrecord.version())
				.arg(tes3_header_subrecord.unk())
				.arg(tes3_header_subrecord.author())
				.arg(tes3_header_subrecord.description())
				.arg(tes3_header_subrecord.num_records());
	}
}

void
Visitors::ToQVariantVisitor::visit_tes3_int16_subrecord(
		TES3::Int16SubRecord &tes3_int16_subrecord)
{
	if (d_field == NAME) {
		d_qv = tes3_int16_subrecord.fourchar();
	} else {
		d_qv = QVariant(tes3_int16_subrecord.value());
	}
}

void
Visitors::ToQVariantVisitor::visit_tes3_int32_subrecord(
		TES3::Int32SubRecord &tes3_int32_subrecord)
{
	if (d_field == NAME) {
		d_qv = tes3_int32_subrecord.fourchar();
	} else {
		d_qv = QVariant(tes3_int32_subrecord.value());
	}
}

void
Visitors::ToQVariantVisitor::visit_tes3_int64_subrecord(
		TES3::Int64SubRecord &tes3_int64_subrecord)
{
	if (d_field == NAME) {
		d_qv = tes3_int64_subrecord.fourchar();
	} else {
		d_qv = QVariant(tes3_int64_subrecord.value());
	}
}

void
Visitors::ToQVariantVisitor::visit_tes3_item_count_subrecord(
		TES3::ItemCountSubRecord &tes3_item_count_subrecord)
{
	if (d_field == NAME) {
		d_qv = tes3_item_count_subrecord.fourchar();
	} else {
		d_qv = QString("%1x %2")
				.arg(tes3_item_count_subrecord.count())
				.arg(tes3_item_count_subrecord.value());
	}
}

void
Visitors::ToQVariantVisitor::visit_tes3_script_header_subrecord(
		TES3::ScriptHeaderSubRecord &tes3_script_header_subrecord)
{
	if (d_field == NAME) {
		d_qv = tes3_script_header_subrecord.fourchar();
	} else {
		d_qv = QString("Name: %1\nNumShorts: %2\nNumLongs: %3\nNumFloats: %4\nScriptDataSize: %5\nLocalVarSize: %6")
				.arg(tes3_script_header_subrecord.name())
				.arg(tes3_script_header_subrecord.num_shorts())
				.arg(tes3_script_header_subrecord.num_longs())
				.arg(tes3_script_header_subrecord.num_floats())
				.arg(tes3_script_header_subrecord.script_data_size())
				.arg(tes3_script_header_subrecord.local_var_size());
	}
}

void
Visitors::ToQVariantVisitor::visit_tes3_string_subrecord(
		TES3::StringSubRecord &tes3_string_subrecord)
{
	if (d_field == NAME) {
		d_qv = tes3_string_subrecord.fourchar();
	} else {
		d_qv = tes3_string_subrecord.value();
	}
}

void
Visitors::ToQVariantVisitor::visit_tes3_uninterpreted_subrecord(
		TES3::UninterpretedSubRecord &tes3_uninterpreted_subrecord)
{
	if (d_field == NAME) {
		d_qv = tes3_uninterpreted_subrecord.fourchar();
	} else {
		// Aha, interesting, an uninterpreted subrecord. But what is it?
		// Might it be text?
		if (Utils::might_be_ascii_text(tes3_uninterpreted_subrecord.raw_data())) {
			// It is! (maybe) Joy! We can at least show user what this might be.
			d_qv = QString("Uninterpreted: \"%1\"")
					.arg(Utils::bytes_to_escaped_ascii_text(tes3_uninterpreted_subrecord.raw_data()));
		} else {
			// No idea. Give 'em the hex.
			d_qv = QString("Uninterpreted: 0x%1")
					.arg(QString(tes3_uninterpreted_subrecord.raw_data().toHex()));
		}
	}
}


