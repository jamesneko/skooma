/**
 * $Id: GetIdVisitor.h 35 2011-02-17 02:42:56Z james_neko $
 * $Revision: 35 $
 * $Date: 2011-02-17 13:42:56 +1100 (Thu, 17 Feb 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef VISITORS_GETIDVISITOR_H
#define VISITORS_GETIDVISITOR_H

#include <QString>
#include <QByteArray>
#include "Model/RecordVisitor.h"



namespace Visitors
{
	/**
	 * Visitor to extract the unique ID (if applicable) from a record/subrecord for
	 * display by the gui model and maybe linking stuff up later.
	 *
	 * Will recurse into a Record's SubRecords if applicable, in order to ID the record.
	 */
	class GetIdVisitor :
			public Model::RecordVisitor
	{
	public:

		explicit
		GetIdVisitor()
		{  }
		
		virtual
		~GetIdVisitor()
		{  }


		/**
		 * Can and will be the uninitialised string. Check .isNull()
		 */
		QString
		get()
		{
			return d_id;
		}
		
		
		
		virtual
		void
		visit_tes3_record(
				TES3::Record &tes3_record);


		virtual
		void
		visit_tes3_script_header_subrecord(
				TES3::ScriptHeaderSubRecord &tes3_script_header_subrecord);

		virtual
		void
		visit_tes3_string_subrecord(
				TES3::StringSubRecord &tes3_string_subrecord);

	
	private:
	
		/**
		 * The ID we've decided upon.
		 */
		QString d_id;
		
		/**
		 * Cached while visiting TES3 record, to help the subrecord visit methods decide.
		 * Not applicable when visiting other record types.
		 */
		QByteArray d_current_record_fourchar;
	};
}



#endif // VISITORS_RECORDVISITOR_H

