#!/usr/bin/perl

use strict;
use warnings;

my $program = "Skooma";
my $author = "James Clark";
my $year = (localtime())[5] + 1900;

my $defaultbaseclass = "QWidget";

sub main
{
	unless (@ARGV) {
		print "Usage: $0 NamespaceDir/ClassName:BaseClass\n";
	}

	foreach my $name (@ARGV) {
		my ($dirname, $classname, $baseclass);
		if ($name =~ m!^(\w+)/(\w+)(\.(h|cc))?$!) {
			$dirname = $1;
			$classname = $2;
			$baseclass = $defaultbaseclass;
		} elsif ($name =~ m!^(\w+)/(\w+)(\.(h|cc))?:([\w:]*)$!) {
			$dirname = $1;
			$classname = $2;
			$baseclass = $5;
		} else {
			die "Argument '$name' looks weird, please re-specify.\n";
		}
		
		my $namespace = $dirname;
		my $headerfile = "$classname.h";
		my $classfile = "$classname.cc";
		
		my $headerfilecontents = headerfile($classname, $baseclass, $headerfile, $namespace);
		writefile("$dirname/$headerfile", $headerfilecontents);
		my $classfilecontents = classfile($classname, $baseclass, $headerfile, $namespace);
		writefile("$dirname/$classfile", $classfilecontents);
	}

	print "Done. Remember to add files to SVN and build system.\n";
}


sub writefile
{
	my ($filename, $contents) = @_;
	-f $filename and die "File $filename already exists, will not overwrite!\n";
	print STDERR "Writing $filename...\n";
	open(FILE, ">$filename") or die "Couldn't open $filename, $!\n";
	print FILE $contents;
	close(FILE) or die "Couldn't close $filename, $!\n";
}


sub classfile
{
	my ($classname, $baseclass, $headerfile, $namespace) = @_;

	my $ctor = "{  }";
	$ctor = "{\n	setupUi(this);\n}" if ($baseclass =~ /^Q.*(Widget|Window|Dialog)$/);
	my $parentparam = "";
	$parentparam = "\n		QObject *parent_" if ($baseclass =~ /^Q.*$/);
	$parentparam = "\n		QWidget *parent_" if ($baseclass =~ /^Q.*(Widget|Window|Dialog)$/);
	my $parentinit = "";
	$parentinit = ":\n	$baseclass(parent_)" if ($baseclass =~ /^Q.*$/);
	my $string;
	$string .= boilerplate();
	$string .= <<EOF;

#include "$headerfile"


${namespace}::${classname}::${classname}($parentparam)$parentinit
$ctor

EOF
	return $string;
}


sub headerfile
{
	my ($classname, $baseclass, $headerfile, $namespace) = @_;

	my $includeguard = uc("${namespace}_${classname}_H");
	my $widgetclass = "Ui_$classname";
	my $widgetheader = "ui_$headerfile";
	
	my $extends = "";
	$extends = " :\n			public $baseclass" if ($baseclass ne "");
	$extends .= ",\n			protected $widgetclass" if ($baseclass =~ /^Q.*(Widget|Window|Dialog)$/);

	my $includewidgetline = "";
	$includewidgetline = "#include \"$widgetheader\"" if ($baseclass =~ /^Q.*(Widget|Window|Dialog)$/);
	
	my $qobjectmacro = "";
	$qobjectmacro = "\n		Q_OBJECT\n" if ($baseclass =~ /^Q.*$/);
	
	my $parentparam = "";
	$parentparam = "\n				QObject *parent_" if ($baseclass =~ /^Q.*$/);
	$parentparam = "\n				QWidget *parent_" if ($baseclass =~ /^Q.*(Widget|Window|Dialog)$/);

	my $string;
	$string .= boilerplate();
	$string .= <<EOF;

#ifndef $includeguard
#define $includeguard

#include <$baseclass>

$includewidgetline

namespace $namespace
{
	/**
	 * 
	 */
	class $classname$extends
	{$qobjectmacro
	public:
		explicit
		$classname($parentparam);
		
		virtual
		~$classname()
		{  }
		
	private:
		
	};
}

#endif // $includeguard

EOF
	return $string;
}


sub boilerplate
{
	my $string = <<EOF;
/**
 * \$Id\$
 * \$Revision\$
 * \$Date\$ 
 * 
 * Copyright (C) $year $author
 *
 * This file is part of $program.
 *
 * $program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * $program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
EOF
	return $string;
}


main();

