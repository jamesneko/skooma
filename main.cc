/**
 * $Id: main.cc 49 2011-12-07 00:48:54Z james_neko $
 * $Revision: 49 $
 * $Date: 2011-12-07 11:48:54 +1100 (Wed, 07 Dec 2011) $ 
 * 
 * Copyright (C) 2008, 2009, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QApplication>
#include <QDebug>

#include "Gui/MainWindow.h"

/**
 * FIXME:
 * Some sort of a RecordComparisonVisitor.
 *
 * TODO:
 * Actually incorporate this new IdIndex thing.
 */

int main(int argc, char *argv[])
{
	Q_INIT_RESOURCE(skooma);
	QApplication app(argc, argv);
	
	qDebug() << "Starting Skooma.";
	qDebug() << "Qt Library paths:" << QApplication::libraryPaths();
	
	QStringList filenames = app.arguments();
	filenames.pop_front();
	Gui::MainWindow *mw = new Gui::MainWindow();
	mw->show();
	mw->read_files(filenames);

	return app.exec();
}


