# README #

Skooma is a pretty old project I started to edit TES3 data files. In its present form it can do basic viewing of the raw record data. I had ambitions to make it generic enough to then handle TES4 and TES5 data - perhaps it is of interest to someone in its present form, anyway.