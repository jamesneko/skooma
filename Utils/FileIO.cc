/**
 * $Id: FileIO.cc 51 2012-03-05 01:07:11Z james_neko $
 * $Revision: 51 $
 * $Date: 2012-03-05 12:07:11 +1100 (Mon, 05 Mar 2012) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "FileIO.h"


void
Utils::make_datastream_sane(
		QDataStream &data_stream)
{
	// Because ESP files are most useful exclusively on little-endian systems,
	// it is reasonable to assume that the file will always be in this
	// format, regardless of the platform it is being read/written on.
	data_stream.setByteOrder(QDataStream::LittleEndian);
	// Furthermore, we need to ensure QDataStream does not give us unpredictable behaviour
	// when using future Qt versions; see QDataStream::setFloatingPointPrecision().
	// Don't go above Qt_4_6 or we lose the ability to grab a float and/or double reliably.
	data_stream.setVersion(QDataStream::Qt_4_5);
}


QByteArray
Utils::read_fourchar(
		QDataStream &data_stream)
{
	QByteArray buf(4, '\0');
	int bytesread = data_stream.readRawData(buf.data(), 4);
	if (bytesread < 4) {
		qDebug("read_fourchar(): Error reading bytes.");
	}
	return buf;
}

	
QByteArray
Utils::read_raw_data(
		QDataStream &data_stream,
		quint32 size)
{
	QByteArray buf(size, '\0');
	int bytesread = data_stream.readRawData(buf.data(), size);
	if (static_cast<quint32>(bytesread) < size) {
		qDebug("read_raw_data(): Error reading bytes.");
	}
	return buf;
}

QString
Utils::read_raw_ascii(
		QDataStream &data_stream,
		quint32 size)
{
	QByteArray raw_data = read_raw_data(data_stream, size);
	// FIXME: European versions of Morrowind - do they use ASCII? Latin-1? See also fromLocal8Bit().
	//        Hell, I know Oblivion was localised into Japanese at one point and Skyrim could possibly
	//        be using Unicode finally, but not sure.
	return QString::fromAscii(raw_data.data());
}

