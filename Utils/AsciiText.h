/**
 * $Id: AsciiText.h 51 2012-03-05 01:07:11Z james_neko $
 * $Revision: 51 $
 * $Date: 2012-03-05 12:07:11 +1100 (Mon, 05 Mar 2012) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef UTILS_TEXT_H
#define UTILS_TEXT_H

#include <QByteArray>


namespace Utils
{
	static const char bytearray_escape_char = '\\';
	
	/**
	 * Used for might_be_ascii_text(). Returns an index of what constitutes valid 'ascii text'
	 */
	QByteArray
	get_bytearray_ascii_text_chars()
	{
		QByteArray text(256, ' ');
		int i = 0;
		text[i++] = '\t';
		text[i++] = '\012'; // dec 10 LF
		text[i++] = '\015'; // dec 13 CR
		for (char c = 32; c < 126; ++c) {
			if (c != bytearray_escape_char) {
				text[i] = c;
				++i;
			}
		}
		return text;
	}

	
	/**
	 * Heuristic to guess at the asciiness of some byte sequence.
	 */
	bool
	might_be_ascii_text(
			const QByteArray &data)
	{
		static const QByteArray text_chars = get_bytearray_ascii_text_chars();
		
		// Must be longer than 4 chars, lest we run into some small data value.
		if (data.size() <= 4) {
			return false;
		}
		
		// If the first 8 bytes are within the ascii text range, let's assume text.
		for (int i = 0; i < data.size() && i < 8; ++i) {
			if ( ! text_chars.contains(data[i])) {
				return false;
			}
		}
		return true;
	}


	/**
	 * Escapes unprintable or otherwise non-human-readable might-be-binary ascii characters,
	 * so it won't horribly scar the reader's eyes. Might be over-zealous.
	 */
	QString
	bytes_to_escaped_ascii_text(
			const QByteArray &data)
	{
		static const QByteArray text_chars = get_bytearray_ascii_text_chars();
		
		// Use a modified URL-style 'percent' encoding.
		QByteArray encoded = data.toPercentEncoding(text_chars, "", bytearray_escape_char);
		return QString::fromAscii(encoded);
	}

}



#endif // UTILS_TEXT_H
