/**
 * $Id: FileIO.h 34 2011-02-13 07:24:22Z james_neko $
 * $Revision: 34 $
 * $Date: 2011-02-13 18:24:22 +1100 (Sun, 13 Feb 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef UTILS_FILEIO_H
#define UTILS_FILEIO_H

#include <QDataStream>
#include <QByteArray>

namespace Utils
{
	void
	make_datastream_sane(
			QDataStream &data_stream);
	

	QByteArray
	read_fourchar(
			QDataStream &data_stream);
	
	QByteArray
	read_raw_data(
			QDataStream &data_stream,
			quint32 size);
	
	QString
	read_raw_ascii(
			QDataStream &data_stream,
			quint32 size);

}

#endif // UTILS_FILEIO_H

