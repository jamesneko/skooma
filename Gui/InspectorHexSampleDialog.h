/**
 * $Id: InspectorHexSampleDialog.h 41 2011-05-07 00:43:39Z james_neko $
 * $Revision: 41 $
 * $Date: 2011-05-07 10:43:39 +1000 (Sat, 07 May 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef GUI_INSPECTORHEXSAMPLEDIALOG_H
#define GUI_INSPECTORHEXSAMPLEDIALOG_H

#include <QDialog>

#include "ui_InspectorHexSampleDialog.h"

namespace Gui
{
	/**
	 * Small dialog to hold the collection of hex samples to be inspected.
	 *
	 * This dialog makes it:s members public for easier access by the parent InspectorHex.
	 */
	class InspectorHexSampleDialog :
			public QDialog,
			public Ui_InspectorHexSampleDialog
	{
		Q_OBJECT

	public:
		explicit
		InspectorHexSampleDialog(
				QWidget *parent_);
		
		virtual
		~InspectorHexSampleDialog()
		{  }
		
		/**
		 * Quicker way to get at the currently selected row index.
		 * Will return -1 if there is no selection.
		 */
		int
		selected_row();
		
	signals:
	
		void
		selection_changed(
				int row,
				QString data,
				QString comment);
	
	public slots:
	
		void
		next();
		
		void
		prev();
	
	private slots:
	
		void
		handle_add_new_sample();
		
		void
		handle_remove_current_sample();
		
		void
		handle_table_change();
		
	private:
		
	};
}

#endif // GUI_INSPECTORHEXSAMPLEDIALOG_H

