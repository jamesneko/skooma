/**
 * $Id: Dialogs.h 49 2011-12-07 00:48:54Z james_neko $
 * $Revision: 49 $
 * $Date: 2011-12-07 11:48:54 +1100 (Wed, 07 Dec 2011) $ 
 * 
 * Copyright (C) 2008, 2009, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef GUI_DIALOGS_H
#define GUI_DIALOGS_H

#include <QObject>
#include <QPointer>
#include <QDialog>
#include <QAction>


namespace Gui
{
	// Forward declaration to reduce dependencies.
	class MainWindow;
	
	class AboutDialog;
	class GrepDialog;
	class InspectorHexWindow;


	/**
	 * The Dialogs object is used to hold pointers to various important top-level dialogs
	 * and windows, typically parented to MainWindow, and provide a nice way to access them
	 * from elsewhere in the application without having to pass everything as arguments.
	 *
	 * It also constructs them but that sounds suspiciously like the Factory pattern so shhhh.
	 */
	class Dialogs :
			public QObject
	{
		Q_OBJECT
		
	public:
		explicit
		Dialogs(
				MainWindow *main_window_ = NULL);
		
		virtual
		~Dialogs();
		
		/**
		 * Use this to gain access to the Dialogs via spooky-action-at-a-distance.
		 */
		static
		Dialogs &
		instance();
		
		/**
		 * Use this to gain access to the MainWindow from anywhere you like.
		 * Still gotta #include it though, ok?
		 */
		static
		MainWindow *
		main_window();


		/**
		 * This lets MainWindow hook up it:s "open dialog" menu actions without needing
		 * to #include every single dialog under the sun.
		 */
		static
		void
		hook_up_menu_item(
				QAction *action,
				const QString &dialog_name);


		static
		AboutDialog *
		about_dialog();
		
		static
		GrepDialog *
		grep_dialog();
		
		static
		InspectorHexWindow *
		inspector_hex_window();

	private:
		
		/**
		 * A static pointer to our own instance, to make Dialogs access easy from other classes.
		 */
		static Dialogs *s_instance_ptr;
		
		/**
		 * Remember the main window, it is our parent and most important.
		 */
		QPointer<MainWindow> d_main_window_ptr;
		
		// Other dialogs we own. We can change some of these to lazy initialisation later if we need to improve app startup speed.
		
		QPointer<Gui::AboutDialog> d_about_dialog_ptr;
		QPointer<Gui::GrepDialog> d_grep_dialog_ptr;
		QPointer<Gui::InspectorHexWindow> d_inspector_hex_window_ptr;
	};
}

#endif // GUI_DIALOGS_H
