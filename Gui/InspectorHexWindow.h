/**
 * $Id: InspectorHexWindow.h 41 2011-05-07 00:43:39Z james_neko $
 * $Revision: 41 $
 * $Date: 2011-05-07 10:43:39 +1000 (Sat, 07 May 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef GUI_INSPECTORHEXWINDOW_H
#define GUI_INSPECTORHEXWINDOW_H

#include <QDialog>

#include "InspectorHexSampleDialog.h"

#include "ui_InspectorHexWindow.h"

namespace Gui
{
	/**
	 * A handy window to examine binary to look for (and possibly assign) meaning.
	 */
	class InspectorHexWindow :
			public QDialog,
			protected Ui_InspectorHexWindow
	{
		Q_OBJECT

	public:
		explicit
		InspectorHexWindow(
				QWidget *parent_);
		
		virtual
		~InspectorHexWindow()
		{  }

	private slots:
	
		void
		react_sample_selection_changed(
				int row,
				QString data,
				QString comment);
		
	private:
	
		/**
		 * Connect the signal/slot relationships for the Sample groupbox.
		 */
		void
		connect_sample_actions();

		/**
		 * Connect the signal/slot relationships for the Selection groupbox.
		 */
		void
		connect_selection_actions();

		/**
		 * Connect the signal/slot relationships for the Interpretation groupbox.
		 */
		void
		connect_interpretation_actions();

		/**
		 * Connect other signal/slot relationships for this window.
		 */
		void
		connect_other_actions();

		
		InspectorHexSampleDialog *d_sample_dialog_ptr;
	};
}

#endif // GUI_INSPECTORHEXWINDOW_H

