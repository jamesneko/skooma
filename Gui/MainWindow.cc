/**
 * $Id: MainWindow.cc 49 2011-12-07 00:48:54Z james_neko $
 * $Revision: 49 $
 * $Date: 2011-12-07 11:48:54 +1100 (Wed, 07 Dec 2011) $ 
 * 
 * Copyright (C) 2009, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Gui/MainWindow.h"


#include "Model/File.h"

#include "Gui/Dialogs.h"
#include "Gui/AboutDialog.h"
#include "Gui/GrepDialog.h"
#include "Gui/InspectorHexWindow.h"

#include "Gui/FileControls.h"


Gui::MainWindow::MainWindow(
		QWidget *parent_):
	QMainWindow(parent_),
	d_dialogs_ptr(new Gui::Dialogs(this)),
	d_file_dialog(this),
	d_progress_dialog(this)
{
	setupUi(this);
	
	// Main list of files.
	QWidget *central = new QWidget;
	setCentralWidget(central);
	d_file_controls_layout = new QVBoxLayout(central);
	d_file_controls_layout->setSpacing(2);
	d_file_controls_layout->setContentsMargins(2, 2, 2, 2);
	
	connect_menu_actions();
}


void
Gui::MainWindow::read_files(
		QStringList filenames)
{
	// Figure out total progress bar size.
	// FIXME: Also figure out if we need to load any dependencies.
	d_progress_bytes_to_read = 0;
	d_progress_base_bytes = 0;
	foreach (QString filename, filenames)
	{
		QFileInfo fileinfo(filename);
		d_progress_bytes_to_read += fileinfo.size();
	}
	d_progress_dialog.setRange(0, d_progress_bytes_to_read);
	d_progress_dialog.setMinimumDuration(2000);
	
	// Then read each file temporarily into a RawRecordsWidget.
	foreach (QString filename, filenames)
	{
		TES3::Reader reader(filename);
		d_progress_dialog.setLabelText(tr("Reading file \"%1\"...").arg(reader.filename()));
		QObject::connect(&reader, SIGNAL(read_progress(qint64)),
				this, SLOT(handle_read_progress(qint64)));

		// A File object to encapsulate this file.
		Model::File file(filename);
		// We get a Model::RecordCollection from the TES3 reader,
		Model::RecordCollection records = reader.read();	// Now using magical QSharedData.
		// Assign the newly-read records to the file.
		*file.records() = records;
		
		// Okay, we've done that bit, update the progress bar's base value.
		d_progress_base_bytes += reader.size();

		// Add it to the GUI list of files open.
		d_file_controls_layout->addWidget(new FileControls(file, this));
	}
	
	// Just in case it didn't do it automatically for some reason.
	d_progress_dialog.reset();
	d_progress_dialog.hide();
}


void
Gui::MainWindow::handle_quit()
{
//	QApplication::closeAllWindows();
	QApplication::quit();
}


void
Gui::MainWindow::closeEvent(
		QCloseEvent *event)
{
	handle_quit();
	event->accept();
}


void
Gui::MainWindow::connect_menu_actions()
{
	// **** File ****
	connect(action_Quit, SIGNAL(triggered()), this, SLOT(handle_quit()));
	connect(action_Open, SIGNAL(triggered()), this, SLOT(handle_open_file_dialog()));
	
	// ****  ****
	d_dialogs_ptr->hook_up_menu_item(action_About, "AboutDialog");
	d_dialogs_ptr->hook_up_menu_item(action_Grep_Files, "GrepDialog");
	d_dialogs_ptr->hook_up_menu_item(action_Inspector_Hex, "InspectorHexWindow");
}


