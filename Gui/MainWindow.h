/**
 * $Id: MainWindow.h 49 2011-12-07 00:48:54Z james_neko $
 * $Revision: 49 $
 * $Date: 2011-12-07 11:48:54 +1100 (Wed, 07 Dec 2011) $ 
 * 
 * Copyright (C) 2008, 2009, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef GUI_MAINWINDOW_H
#define GUI_MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QPointer>
#include <QDebug>
#include <QStringList>
#include <QProgressDialog>
#include <QFileDialog>
#include <QVBoxLayout>

#include "Model/RecordCollection.h"
#include "TES3/Reader.h"

#include "ui_MainWindow.h"


namespace Gui
{
	// Forward declaration out of habit.
	class Dialogs;


	/**
	 * Main window. duh.
	 * Everything has to start here rather than main() because we want the Qt
	 * Event Loop to be set up and ready.
	 */
	class MainWindow :
			public QMainWindow,
			protected Ui_MainWindow
	{
		Q_OBJECT
		
	public:
		explicit
		MainWindow(
				QWidget *parent_ = NULL);
		
		virtual
		~MainWindow()
		{  }
		
	public slots:
		
		void
		read_files(
				QStringList filenames);
	
	private slots:
	
		void
		handle_open_file_dialog()
		{
			QStringList filenames = d_file_dialog.getOpenFileNames();
			read_files(filenames);
		}
		
		void
		handle_read_progress(
				qint64 bytes)
		{
			d_progress_dialog.setValue(static_cast<int>(d_progress_base_bytes + bytes));
			QCoreApplication::processEvents();
		}
		
		void
		handle_quit();
			
	signals:
	
	protected:
	
		void
		closeEvent(
				QCloseEvent *event);
		
	private:
		
		/**
		 * Hook up our menu actions.
		 */
		void
		connect_menu_actions();
		
		
		QPointer<Gui::Dialogs> d_dialogs_ptr;

		QFileDialog d_file_dialog;

		QProgressDialog d_progress_dialog;
		qint64 d_progress_bytes_to_read;
		qint64 d_progress_base_bytes;
		
		QVBoxLayout *d_file_controls_layout;
	};
}

#endif // GUI_MAINWINDOW_H
