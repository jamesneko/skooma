/**
 * $Id: RecordsFilterModel.cc 38 2011-03-05 16:24:39Z james_neko $
 * $Revision: 38 $
 * $Date: 2011-03-06 03:24:39 +1100 (Sun, 06 Mar 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QRegExp>
#include "RecordsFilterModel.h"


Gui::RecordsFilterModel::RecordsFilterModel(
		QObject *parent_):
	QSortFilterProxyModel(parent_)
{  }


void
Gui::RecordsFilterModel::set_filter_text(
		const QString &filter_text)
{
	// Sets the base class's filterRegExp() property as though
	// QRegExp("text", Qt::CaseInsensitive, QRegExp::FixedString) were called.
	setFilterCaseSensitivity(Qt::CaseInsensitive);
	setFilterFixedString(filter_text);
}



bool
Gui::RecordsFilterModel::filterAcceptsRow(
		int source_row,
		const QModelIndex &source_parent) const
{
	if (source_parent.isValid()) {
		// We are checking to see if the filter accepts a child (subrecord) node.
		// Now, so that the user is always shown the whole picture, we want this
		// to match all the time, so that as long as the top-level (record) node
		// is being shown, the subrecords contained in there will be shown too.
		// Actual testing of subrecord nodes is done in the case that we are
		// looking at the parent top-level node, below.
		return true;
	} else {
		// We are checking to see if the filter accepts a top-level (record) node.
		// The trick here is, we want to match top-level records that match the
		// filter string for e.g. types and IDs, but we also want this node to
		// match if any child nodes match as well, so that filter queries work
		// for the 'guts' of a record.
		QModelIndex index_rnode = sourceModel()->index(source_row, 0, source_parent);
		int children = sourceModel()->rowCount(index_rnode);
		for (int r = 0; r < children; ++r) {
			if (match_row(r, index_rnode)) {
				return true;
			}
		}
		return false;
	}
}


bool
Gui::RecordsFilterModel::match_row(
		int source_row,
		const QModelIndex &source_parent) const
{
	// FIXME: Adapt this to scan all columns not just 0-1.
	QModelIndex index_rtype = sourceModel()->index(source_row, 0, source_parent);
	QModelIndex index_rval = sourceModel()->index(source_row, 1, source_parent);
	
	QString rtype = sourceModel()->data(index_rtype).toString();
	QString rval = sourceModel()->data(index_rval).toString();
	
	return (rtype.contains(filterRegExp()) || rval.contains(filterRegExp()));
}


