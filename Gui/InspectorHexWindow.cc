/**
 * $Id: InspectorHexWindow.cc 41 2011-05-07 00:43:39Z james_neko $
 * $Revision: 41 $
 * $Date: 2011-05-07 10:43:39 +1000 (Sat, 07 May 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "InspectorHexWindow.h"


Gui::InspectorHexWindow::InspectorHexWindow(
		QWidget *parent_):
	QDialog(parent_),
	d_sample_dialog_ptr(new InspectorHexSampleDialog(this))
{
	setupUi(this);
	connect_sample_actions();
	connect_selection_actions();
	connect_interpretation_actions();
	connect_other_actions();
}



void
Gui::InspectorHexWindow::react_sample_selection_changed(
	int row,
	QString data,
	QString comment)
{
	if (row >= 0) {
		w_sample_text->setText(data);		// FIXME: actual encodings
		w_sample_comment->setText(tr("Sample #%1: %2").arg(row+1).arg(comment));
	} else {
		w_sample_text->clear();
		w_sample_comment->setText(tr("No sample loaded to inspect."));
	}
}


void
Gui::InspectorHexWindow::connect_sample_actions()
{
	// Handle our buttons.
	connect(w_sample_prev, SIGNAL(clicked()),
			d_sample_dialog_ptr, SLOT(prev()));
	connect(w_sample_next, SIGNAL(clicked()),
			d_sample_dialog_ptr, SLOT(next()));
	connect(w_manage_samples, SIGNAL(clicked()),
			d_sample_dialog_ptr, SLOT(show()));

	// React to our SampleDialog.
	connect(d_sample_dialog_ptr, SIGNAL(selection_changed(int, QString, QString)),
			this, SLOT(react_sample_selection_changed(int, QString, QString)));
}

void
Gui::InspectorHexWindow::connect_selection_actions()
{
}

void
Gui::InspectorHexWindow::connect_interpretation_actions()
{
}

void
Gui::InspectorHexWindow::connect_other_actions()
{
}



