/**
 * $Id: RawRecordsModel.cc 46 2011-07-13 23:21:15Z james_neko $
 * $Revision: 46 $
 * $Date: 2011-07-14 09:21:15 +1000 (Thu, 14 Jul 2011) $ 
 * 
 * Copyright (C) 2008, 2009, 2010, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QDebug>

#include <QWidget>
#include <QVariant>
#include "RawRecordsModel.h"

#include "Model/AbstractRecord.h"
#include "TES3/Record.h"
#include "TES3/SubRecord.h"
#include "Visitors/ToQVariantVisitor.h"


namespace
{
	enum ColumnName
	{
		COLUMN_TYPE = 0, COLUMN_MISC = 1, NUMBER_OF_COLUMNS
	};
	
	
	enum RecordType
	{
		UNKNOWN, RECORD, SUBRECORD
	};
	
	
	/**
	 * Determines if we need to draw this record as a top-level item,
	 * or as a subitem.
	 */
	RecordType
	figure_out_record_type(
			Model::AbstractRecord *arecord_ptr)
	{
		if (arecord_ptr->parent()) {
			return SUBRECORD;
		} else {
			return RECORD;
		}
	}	
	
}


Gui::RawRecordsModel::RawRecordsModel(
		Gui::RawRecordsModel::records_shared_ptr records_ptr,
		QObject *parent_):
	QAbstractItemModel(parent_),
	d_records_ptr(records_ptr)
{
	// FIXME: Do something to set up our model data.
	
}


QModelIndex
Gui::RawRecordsModel::index(
		int row,
		int column,
		const QModelIndex &parent) const
{
	if ( ! hasIndex(row, column, parent)) {
		// Don't return anything out-of-bounds.
		return QModelIndex();
	}
	
	if ( ! parent.isValid())
	{
		// The parent index refers to the invisible root item.
		// So we need to return one of the top-level items.
		Model::AbstractRecord *arecord_ptr = d_records_ptr->at(row);
		return createIndex(row, column, arecord_ptr);
	}
	else
	{
		// The parent index refers to a valid entry in the tree.
		// So we need to get it, and figure out it's children.
		Model::AbstractRecord *arecord_ptr = static_cast<Model::AbstractRecord *>(parent.internalPointer());
		// FIXME: Might want to turn this into a visitor.
		if (arecord_ptr->parent()) {
			// The parent index is a subrecord and has no children.
			return QModelIndex();
		} else {
			// The parent index is a record and may have children.
			TES3::Record *record_ptr = static_cast<TES3::Record *>(arecord_ptr);
			QList<TES3::SubRecord *> &subrecords = record_ptr->subrecords();
			if (row >= 0 && row < subrecords.size()) {
				TES3::SubRecord *subrecord_ptr = subrecords[row];
				return createIndex(row, column, subrecord_ptr);
			} else {
				return QModelIndex();
			}
		}
	}
}


QModelIndex
Gui::RawRecordsModel::parent(
		const QModelIndex &index) const
{
	if ( ! index.isValid()) {
		// The given index is the invisible root item, which has no parents.
		return QModelIndex();
	}
	
	// If index refers to a subrecord, we need to figure out which
	// record index to return.
	Model::AbstractRecord *arecord_ptr = static_cast<Model::AbstractRecord *>(index.internalPointer());
	// FIXME: Might want to turn this into a visitor.
	if (arecord_ptr->parent()) {
		// The given index is a subrecord and has a parent.
		Model::AbstractRecord *parent_record_ptr = static_cast<Model::AbstractRecord *>(arecord_ptr->parent());
		records_type::records_iterator it = d_records_ptr->begin();
		records_type::records_iterator end = d_records_ptr->end();
		for (int record_row = 0 ; it != end; ++it, ++record_row)
		{
			if (*it == parent_record_ptr) {
				return createIndex(record_row, 0, parent_record_ptr);
			}
		}
		return QModelIndex();
	} else {
		// else, it's a record, and the parent is the invisible root item.
		return QModelIndex();
	}
}


int
Gui::RawRecordsModel::rowCount(
		const QModelIndex &parent) const
{
	if ( ! parent.isValid()) {
		// The given parent index is the invisible root item, so we need to
		// return the number of top level items.
		return d_records_ptr->size();
	} else {
		// The given parent index refers to a record or a subrecord.
		// Figure out which and return record's subrecord count, or 0.
		Model::AbstractRecord *arecord_ptr = static_cast<Model::AbstractRecord *>(parent.internalPointer());
		// FIXME: Might want to turn this into a visitor.
		if (arecord_ptr->parent()) {
			// The parent index is a subrecord and has no children.
			return 0;
		} else {
			// The parent index is a record and may have children.
			TES3::Record *record_ptr = static_cast<TES3::Record *>(arecord_ptr);
			QList<TES3::SubRecord *> &subrecords = record_ptr->subrecords();
			return subrecords.size();
		}
	}
}


int
Gui::RawRecordsModel::columnCount(
		const QModelIndex &) const
{
	return NUMBER_OF_COLUMNS;
}


QVariant
Gui::RawRecordsModel::data(
		const QModelIndex &index,
		int model_role) const
{
	if ( ! index.isValid()) {
		return QVariant();
	}
	
	if (model_role == Qt::DisplayRole) {
		
		if (index.internalPointer() != NULL) {
			Model::AbstractRecord *arecord_ptr = static_cast<Model::AbstractRecord *>(index.internalPointer());
			
			// Figure out which ToQVariantVisitor field to use for this column.
			Visitors::ToQVariantVisitor::Field record_field = Visitors::ToQVariantVisitor::NAME;
			if (index.column() == COLUMN_TYPE) {
				record_field = Visitors::ToQVariantVisitor::NAME;
			} else if (index.column() == COLUMN_MISC) {
				record_field = Visitors::ToQVariantVisitor::VALUE;
			}
			
			// Ask the visitor nicely for a value we can return to the QTreeView.
			Visitors::ToQVariantVisitor toqv(record_field, model_role);
			arecord_ptr->accept_visitor(toqv);
			return toqv.get();

		} else {
			qDebug() << "data(): index.internalPointer() is NULL!";
			return QVariant();
		}
		
	} else {
		return QVariant();
	}
}


Qt::ItemFlags
Gui::RawRecordsModel::flags(
		const QModelIndex &index) const
{
	if ( ! index.isValid()) {
		return 0;
	}
	
	return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}


QVariant
Gui::RawRecordsModel::headerData(
		int section,
		Qt::Orientation orientation,
		int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
		if (section == COLUMN_TYPE) {
			return tr("Record type");
		} else if (section == COLUMN_MISC) {
			return tr("Value");
		} else {
			return QVariant();
		}
	}
	return QVariant();
}



