/**
 * $Id: RawRecordsWidget.h 45 2011-07-13 21:49:14Z james_neko $
 * $Revision: 45 $
 * $Date: 2011-07-14 07:49:14 +1000 (Thu, 14 Jul 2011) $ 
 * 
 * Copyright (C) 2008, 2009, 2010, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef GUI_RAWRECORDSWIDGET_H
#define GUI_RAWRECORDSWIDGET_H

#include <QWidget>
#include <QMenu>
#include <QPoint>
#include <QPointer>
#include <QTimer>

#include "RawRecordsModel.h"
#include "RecordsFilterModel.h"

#include "ui_RawRecordsWidget.h"

namespace Gui
{
	/**
	 * Tabular view of raw record & subrecord data.
	 */
	class RawRecordsWidget :
			public QWidget,
			protected Ui_RawRecordsWidget
	{
		Q_OBJECT
		
	public:
		explicit
		RawRecordsWidget(
				QWidget *parent_);

		void
		display_records(
				Gui::RawRecordsModel::records_shared_ptr records_ptr);
	
	private slots:
	
		/**
		 * Called when we get text change events from the filter LineEdit.
		 * After a short timeout, it will call handle_filter_changed(),
		 * but continuous typing will keep resetting the timer.
		 */
		void
		handle_filter_typing();

		/**
		 * Called indirectly after the d_filter_timeout timer reaches 0
		 * indicating the user has finished typing.
		 */
		void
		handle_filter_changed();
		
		/**
		 * The popup for the TreeView.
		 */
		void
		popup_context_menu(
				const QPoint &pos);
		
	private:
		
		Gui::RawRecordsModel::records_shared_ptr d_records_ptr;
		QPointer<RawRecordsModel> d_records_model_ptr;
		QPointer<RecordsFilterModel> d_records_filter_model_ptr;
		
		QPointer<QTimer> d_filter_timeout;
		QPointer<QMenu> d_context_menu;
	};
}

#endif // GUI_RAWRECORDSWIDGET_H
