/**
 * $Id: InspectorHexSampleDialog.cc 41 2011-05-07 00:43:39Z james_neko $
 * $Revision: 41 $
 * $Date: 2011-05-07 10:43:39 +1000 (Sat, 07 May 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QDebug>

#include "InspectorHexSampleDialog.h"


Gui::InspectorHexSampleDialog::InspectorHexSampleDialog(
		QWidget *parent_):
	QDialog(parent_)
{
	setupUi(this);
	
	// Our buttons.
	connect(w_add_new_sample, SIGNAL(clicked()),
			this, SLOT(handle_add_new_sample()));
	connect(w_remove_sample, SIGNAL(clicked()),
			this, SLOT(handle_remove_current_sample()));
	
	// Unify the change signals from the table for Rex, I mean, Hex to listen to.
	connect(w_sample_table, SIGNAL(itemChanged(QTableWidgetItem *)),
			this, SLOT(handle_table_change()));
	connect(w_sample_table, SIGNAL(itemSelectionChanged()),
			this, SLOT(handle_table_change()));
}


int
Gui::InspectorHexSampleDialog::selected_row()
{
	QList<QTableWidgetSelectionRange> selections = w_sample_table->selectedRanges();
	if (selections.size() > 0) {
		return selections.at(0).topRow();
	} else {
		return -1;
	}
}


void
Gui::InspectorHexSampleDialog::next()
{
	int row = selected_row();
	if (row >= 0) {
		w_sample_table->setCurrentCell(++row % w_sample_table->rowCount(), 0);
	}
}

void
Gui::InspectorHexSampleDialog::prev()
{
	int row = selected_row();
	if (row > 0) {
		w_sample_table->setCurrentCell(--row % w_sample_table->rowCount(), 0);
	} else if (row == 0) {
		w_sample_table->setCurrentCell(w_sample_table->rowCount() - 1, 0);
	}
}


void
Gui::InspectorHexSampleDialog::handle_add_new_sample()
{
	// Create it.
	int row = w_sample_table->rowCount();
	w_sample_table->insertRow(row);
	QTableWidgetItem *data = new QTableWidgetItem();
	w_sample_table->setItem(row, 0, data);
	w_sample_table->setItem(row, 1, new QTableWidgetItem());
	
	// Then scroll to it and edit.
	w_sample_table->setCurrentItem(data);
	w_sample_table->scrollToItem(data);
	w_sample_table->editItem(data);
}

void
Gui::InspectorHexSampleDialog::handle_remove_current_sample()
{
	int row = selected_row();
	if (row >= 0) {
		w_sample_table->removeRow(row);
	}
}


void
Gui::InspectorHexSampleDialog::handle_table_change()
{
	// What row is selected?
	QList<QTableWidgetSelectionRange> selections = w_sample_table->selectedRanges();
	if (selections.size() == 0) {
		emit selection_changed(-1, "", "");
		return;
	}
	int row = selections.at(0).topRow();
	
	QString data = w_sample_table->item(row, 0)->data(Qt::EditRole).toString();
	QString comment = w_sample_table->item(row, 1)->data(Qt::EditRole).toString();
	emit selection_changed(row, data, comment);
}

