/**
 * $Id: AboutDialog.cc 15 2008-06-30 16:31:00Z james_neko $
 * $Revision: 15 $
 * $Date: 2008-07-01 02:31:00 +1000 (Tue, 01 Jul 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QFont>
#include <QFile>
#include <QTextStream>

#include "Gui/AboutDialog.h"

namespace
{
	void
	load(
			QTextEdit *textedit,
			const QString &resource)
	{
		static QFont font("Monospace");
		
		QFile infile(resource);
		infile.open(QIODevice::ReadOnly);
		QTextStream instream(&infile);
		QString text = instream.readAll();
		infile.close();
		
		textedit->setFont(font);
		textedit->setUndoRedoEnabled(false);
		textedit->setPlainText(text);
	}
}


Gui::AboutDialog::AboutDialog(
		QWidget *parent_):
	QDialog(parent_)
{
	setupUi(this);
	
	// SVN_HEAD_REVISION is defined via some qmake magic and the svnversion program.
#ifdef SVN_HEAD_REVISION
	w_label_revision->setText("Revision " SVN_HEAD_REVISION);
#endif
	
	// The licences are referenced by the Resources/skooma.qrc file,
	// but it appears ../ is removed from the path.
	load(w_textedit_details, ":/LICENSE");
	load(w_textedit_gpl, ":/LICENSE.GPL");
	load(w_textedit_cc, ":/LICENSE.CC");
	connect(w_button_licence, SIGNAL(toggled(bool)), this, SLOT(show_licence(bool)));
}


void
Gui::AboutDialog::show_licence(
		bool show)
{
	if (show) {
		w_stack->setCurrentIndex(1);
	} else {
		w_stack->setCurrentIndex(0);
	}
}

