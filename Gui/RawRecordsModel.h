/**
 * $Id: RawRecordsModel.h 46 2011-07-13 23:21:15Z james_neko $
 * $Revision: 46 $
 * $Date: 2011-07-14 09:21:15 +1000 (Thu, 14 Jul 2011) $ 
 * 
 * Copyright (C) 2008, 2009, 2010, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef GUI_RAWRECORDSMODEL_H
#define GUI_RAWRECORDSMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QList>

#include "Model/RecordCollection.h"
#include "TES3/Record.h"


namespace Gui
{
	/**
	 * Model of raw record & subrecord data.
	 */
	class RawRecordsModel :
			public QAbstractItemModel
	{
		Q_OBJECT
		
	public:

		typedef Model::RecordCollection records_type;
		typedef Model::RecordCollection::shared_ptr records_shared_ptr;
		
		explicit
		RawRecordsModel(
				records_shared_ptr records_ptr,
				QObject *parent_);

		QModelIndex
		index(
				int row,
				int column,
				const QModelIndex &parent) const;

		QModelIndex
		parent(
				const QModelIndex &index) const;

		int
		rowCount(
				const QModelIndex &parent) const;

		int
		columnCount(
				const QModelIndex &parent) const;

		QVariant
		data(
				const QModelIndex &index,
				int role) const;

		Qt::ItemFlags
		flags(
				const QModelIndex &index) const;

		QVariant
		headerData(
				int section,
				Qt::Orientation orientation,
				int role) const;

	private:
		
		records_shared_ptr d_records_ptr;
	};
}

#endif // GUI_RAWRECORDSMODEL_H

