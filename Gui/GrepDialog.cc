/**
 * $Id: GrepDialog.cc 21 2008-09-03 17:40:21Z james_neko $
 * $Revision: 21 $
 * $Date: 2008-09-04 03:40:21 +1000 (Thu, 04 Sep 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QDialog>
#include "GrepDialog.h"


Gui::GrepDialog::GrepDialog(
		QWidget *parent_):
	QDialog(parent_)
{
	setupUi(this);
}


