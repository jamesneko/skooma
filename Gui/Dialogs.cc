/**
 * $Id: Dialogs.cc 49 2011-12-07 00:48:54Z james_neko $
 * $Revision: 49 $
 * $Date: 2011-12-07 11:48:54 +1100 (Wed, 07 Dec 2011) $ 
 * 
 * Copyright (C) 2009, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QDebug>
#include <QtGlobal>

#include "Gui/Dialogs.h"

#include "Gui/MainWindow.h"
#include "Gui/AboutDialog.h"
#include "Gui/GrepDialog.h"
#include "Gui/InspectorHexWindow.h"


// Our static pointer to help locate ourself.
// Pointer is for reference only; it starts out NULL, and needs to be checked by the
// static accessor; furthermore, our memory is managed by Qt as Dialogs is parented to
// MainWindow.
Gui::Dialogs *Gui::Dialogs::s_instance_ptr = NULL;


Gui::Dialogs::Dialogs(
		Gui::MainWindow *main_window_):
	QObject(main_window_),
	d_main_window_ptr(main_window_)
{
	// First we have to set up the static pointer to ourselves.
	Q_ASSERT(s_instance_ptr == NULL);
	s_instance_ptr = this;
	
	// We also need to construct those dialogs which need to be around at Skooma startup.
	d_about_dialog_ptr = new AboutDialog(main_window_);
	d_grep_dialog_ptr = new GrepDialog(main_window_);
	d_inspector_hex_window_ptr = new InspectorHexWindow(main_window_);
}


Gui::Dialogs::~Dialogs()
{
	Q_ASSERT(s_instance_ptr != NULL);
	s_instance_ptr = NULL;
}


Gui::Dialogs &
Gui::Dialogs::instance()
{
	Q_ASSERT(s_instance_ptr != NULL);
	return *s_instance_ptr;
}


Gui::MainWindow *
Gui::Dialogs::main_window()
{
	return instance().d_main_window_ptr;
}


void
Gui::Dialogs::hook_up_menu_item(
		QAction *action,
		const QString &dialog_name)
{
	QDialog *dialog = instance().d_main_window_ptr->findChild<QDialog *>(dialog_name);
	if (dialog) {
		// We always hook it up to show(); set the 'modal' property on the dialog itself if
		// that's how it should behave.
		QObject::connect(action, SIGNAL(triggered()), dialog, SLOT(show()));
		
	} else {
		qWarning() << "Gui::Dialogs::hook_up_menu_item(): Can't find dialog named"<<dialog_name;
	}
}


Gui::AboutDialog *
Gui::Dialogs::about_dialog()
{
	return instance().d_about_dialog_ptr;
}


Gui::GrepDialog *
Gui::Dialogs::grep_dialog()
{
	return instance().d_grep_dialog_ptr;
}


Gui::InspectorHexWindow *
Gui::Dialogs::inspector_hex_window()
{
	return instance().d_inspector_hex_window_ptr;
}

