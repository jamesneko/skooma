/**
 * $Id: FileControls.h 34 2011-02-13 07:24:22Z james_neko $
 * $Revision: 34 $
 * $Date: 2011-02-13 18:24:22 +1100 (Sun, 13 Feb 2011) $ 
 * 
 * Copyright (C) 2009, 2010, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef GUI_FILECONTROLS_H
#define GUI_FILECONTROLS_H

#include <QWidget>
#include <QPointer>

#include "Model/File.h"

#include "RawRecordsWidget.h"

#include "ui_FileControls.h"

namespace Gui
{
	/**
	 * View of raw record & subrecord data.
	 */
	class FileControls :
			public QWidget,
			protected Ui_FileControls
	{
		Q_OBJECT
		
	public:
		explicit
		FileControls(
				Model::File file,
				QWidget *parent_);
	
	public slots:
	
		void
		open_raw_records_widget();

		/**
		 * Forces all visible windows belonging to this file to be raised to the top
		 * and gain focus.
		 */
		void
		raise_all_windows();
		
	private:
		
		void
		connect_controls();
		
		// FIXME: Change to shared ptr of (File in a FileCollection) later.
		Model::File d_file;
		
		QPointer<Gui::RawRecordsWidget> d_raw_records_widget;
	};
}

#endif // GUI_FILECONTROLS_H
