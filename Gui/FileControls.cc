/**
 * $Id: FileControls.cc 50 2012-01-08 09:57:44Z james_neko $
 * $Revision: 50 $
 * $Date: 2012-01-08 20:57:44 +1100 (Sun, 08 Jan 2012) $ 
 * 
 * Copyright (C) 2009, 2010, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QWidget>
#include "FileControls.h"



Gui::FileControls::FileControls(
		Model::File file,
		QWidget *parent_):
	QWidget(parent_),
	d_file(file)
{
	setupUi(this);
	w_label_basename->setText(file.filename());
	w_label_dirname->setText(file.path());
	// Set file icon. FIXME: Put this logic in some sort of "handled file types" class.
	if (file.suffix() == "esm") {
		w_button_select->setIcon(QIcon(":/Neko/MorrowindIcon1.svg"));
	} else if (file.suffix() == "esp") {
		w_button_select->setIcon(QIcon(":/Neko/MorrowindIcon2.svg"));
	} else if (file.suffix() == "ess") {
		w_button_select->setIcon(QIcon(":/Neko/MorrowindIcon3.svg"));
	} else if (file.suffix() == "trpg") {
		w_button_select->setIcon(QIcon(":/Neko/TRPG-System-d20.svg"));
	}
	connect_controls();
}


void
Gui::FileControls::open_raw_records_widget()
{
	if ( ! d_raw_records_widget) {
		// FIXME: Creating RawRecordsWidget with NULL for now so we get a window instead.
		d_raw_records_widget = new Gui::RawRecordsWidget(NULL);
		// And we give the Model::RecordCollection (a Gui::RawRecordsModel::records_shared_ptr or Model::RecordCollection::shared_ptr) to the viewer.
		d_raw_records_widget->display_records(d_file.records());
	}
	d_raw_records_widget->show();
	d_raw_records_widget->activateWindow();	
	d_raw_records_widget->raise();
}


void
Gui::FileControls::raise_all_windows()
{
	if (d_raw_records_widget && d_raw_records_widget->isVisible()) {
		d_raw_records_widget->activateWindow();	
		d_raw_records_widget->raise();
	}
}


void
Gui::FileControls::connect_controls()
{
	connect(w_button_records, SIGNAL(clicked()), this, SLOT(open_raw_records_widget()));
	connect(w_button_select, SIGNAL(clicked()), this, SLOT(raise_all_windows()));
}


