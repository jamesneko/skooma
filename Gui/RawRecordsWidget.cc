/**
 * $Id: RawRecordsWidget.cc 47 2011-07-18 06:11:55Z james_neko $
 * $Revision: 47 $
 * $Date: 2011-07-18 16:11:55 +1000 (Mon, 18 Jul 2011) $ 
 * 
 * Copyright (C) 2008, 2009, 2010, 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QWidget>
#include <QAction>
#include <QtGlobal>
#include <QDebug>
#include "RawRecordsWidget.h"


Gui::RawRecordsWidget::RawRecordsWidget(
		QWidget *parent_):
	QWidget(parent_),
	d_filter_timeout(new QTimer(this)),
	d_context_menu(new QMenu(this))
{
	setupUi(this);
#if QT_VERSION >= 0x040700
	w_filter->setPlaceholderText(tr("Filter"));	// Available in 4.7 and up.
#endif
	
	// Filtering records on the fly with a Filter lineedit box:
	connect(w_filter, SIGNAL(textChanged(QString)), this, SLOT(handle_filter_typing()));
	connect(w_filter, SIGNAL(returnPressed()), this, SLOT(handle_filter_changed()));
	connect(d_filter_timeout, SIGNAL(timeout()), this, SLOT(handle_filter_changed()));
	d_filter_timeout->setSingleShot(true);
	
	// Context menu for the TreeView to manipulate Records and SubRecords:
	d_context_menu->addAction(tr("Dummy Context Menu Item"));
	
	w_treeview->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(w_treeview, SIGNAL(customContextMenuRequested(const QPoint&)),
			this, SLOT(popup_context_menu(const QPoint &)));
}


void
Gui::RawRecordsWidget::display_records(
		Gui::RawRecordsModel::records_shared_ptr records_ptr)
{
	d_records_ptr = records_ptr;
	// FIXME: We need to figure out the association between records and models
	// and who owns what when.
	d_records_model_ptr = new RawRecordsModel(records_ptr, this);
	
	// The Filter Model is used as a proxy to allow quick searches.
	d_records_filter_model_ptr = new RecordsFilterModel(this);
	d_records_filter_model_ptr->setDynamicSortFilter(true);
	d_records_filter_model_ptr->setSourceModel(d_records_model_ptr);
	
	// The filter model is the one that's actually connected to the tree view widget.
	w_treeview->setModel(d_records_filter_model_ptr);
}


void
Gui::RawRecordsWidget::handle_filter_typing()
{
	// Timer will be started or re-started during typing.
	d_filter_timeout->start(800);
}

void
Gui::RawRecordsWidget::handle_filter_changed()
{
	if (d_records_filter_model_ptr) {
		d_records_filter_model_ptr->set_filter_text(w_filter->text());
	}
}


void
Gui::RawRecordsWidget::popup_context_menu(
		const QPoint &pos)
{
	// Where does the pos map to in the model?
	QModelIndex idx = w_treeview->indexAt(pos);
	qDebug() << "popup_context_menu(): You clicked on" << d_records_filter_model_ptr->data(idx);
	
	// Where does the widget pos map to on the actual screen (i.e. where do we pop up the menu?)
	QPoint globalpos = w_treeview->viewport()->mapToGlobal(pos);
	d_context_menu->exec(globalpos);
}

