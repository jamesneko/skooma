/**
 * $Id: AboutDialog.h 15 2008-06-30 16:31:00Z james_neko $
 * $Revision: 15 $
 * $Date: 2008-07-01 02:31:00 +1000 (Tue, 01 Jul 2008) $ 
 * 
 * Copyright (C) 2008 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef GUI_ABOUTDIALOG_H
#define GUI_ABOUTDIALOG_H

#include <QDialog>

#include "ui_AboutDialog.h"

namespace Gui
{
	/**
	 * About Skooma, and licences.
	 */
	class AboutDialog :
			public QDialog,
			protected Ui_AboutDialog
	{
		Q_OBJECT
		
	public:
		explicit
		AboutDialog(
				QWidget *parent_ = NULL);
		
		virtual
		~AboutDialog()
		{  }
		
	public slots:
		
		void
		show_licence(
				bool show);
		
	};
}

#endif // GUI_ABOUTDIALOG_H
