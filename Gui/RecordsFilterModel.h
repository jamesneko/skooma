/**
 * $Id: RecordsFilterModel.h 38 2011-03-05 16:24:39Z james_neko $
 * $Revision: 38 $
 * $Date: 2011-03-06 03:24:39 +1100 (Sun, 06 Mar 2011) $ 
 * 
 * Copyright (C) 2011 James Clark
 *
 * This file is part of Skooma.
 *
 * Skooma is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by
 * the Free Software Foundation.
 *
 * Skooma is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef GUI_RECORDSFILTERMODEL_H
#define GUI_RECORDSFILTERMODEL_H

#include <QSortFilterProxyModel>



namespace Gui
{
	/**
	 * Proxy Model to filter a RawRecordsModel (or perhaps other record-ish models).
	 */
	class RecordsFilterModel :
			public QSortFilterProxyModel
	{
		Q_OBJECT

	public:
		explicit
		RecordsFilterModel(
				QObject *parent_);
		
		virtual
		~RecordsFilterModel()
		{  }
	
	public slots:
		
		void
		set_filter_text(
				const QString &filter_text);

	protected:
		
		/**
		 * Reimplimentation of QSortFilterProxyModel::filterAcceptsRow().
		 * Our version has to do things a little hackishly, and match top-level rows
		 * by first checking to see if any of the child rows match. This is because
		 * we want the entire set of subrecords showing if a record(+subrecords)
		 * matches the user's text.
		 */
		bool
		filterAcceptsRow(
				int source_row,
				const QModelIndex &source_parent) const;
		
	private:
		
		/**
		 * This one is our own internal row-matching test, called by filterAcceptsRow().
		 * This just consists of the simple "Does it match?" logic, without regard to
		 * dealing with Qt Model/View issues.
		 */
		bool
		match_row(
				int source_row,
				const QModelIndex &source_parent) const;

	};
}

#endif // GUI_RECORDSFILTERMODEL_H

